package org.kp.physicalinventory.controllers;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.PharmacyInfo;
import org.kp.physicalinventory.models.PharmacyListRequest;
import org.kp.physicalinventory.models.PharmacyListResponse;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by is-4586 on 11/2/17.
 */

public class PharmacyListController {

    private static PharmacyListController instance = null;

    public static PharmacyListController getInstance() {
        if (instance == null) {
            instance = new PharmacyListController();
        }
        return instance;
    }

    private PharmacyListController() {
    }

    private PharmacyListResponse pharmacyListResponse;


    public interface PharmacyListFetchCallback {
        void onPharmacyListReceived(PharmacyListResponse pharmacyListResponse);

        void onPharmacyListFetchFailed();

        void onPharmacyListFetchDbDown();

        void onPharmacyListRequestInvalid();

        void onPharmacyListRequestTimeout();
    }


    public boolean hasPharmacyListResponse() {
        return pharmacyListResponse != null;
    }

    public RequestHandle getPharmacyList(final PharmacyListController.PharmacyListFetchCallback callback) {

        final PharmacyListRequest pharmacyListRequest = new PharmacyListRequest();
        Log.d("debug", "Pharmacy list request instance");

        pharmacyListRequest.setDeviceid("");
        pharmacyListRequest.setAppver(null);
        pharmacyListRequest.setAppid(null);
        pharmacyListRequest.setAction("getPharmacies");
        pharmacyListRequest.setMode(null);
        pharmacyListRequest.setRegion(null);
        pharmacyListRequest.setStoreid(null);
        pharmacyListRequest.setUserid(null);
        pharmacyListRequest.setTransactionid(null);

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String httpResponseBody = response.toString();
                Log.d("debug", "The response body is: " + httpResponseBody);
                pharmacyListResponse = JsonParser.ParseJsonForPharmacyList(httpResponseBody);

                if ("000".equals(pharmacyListResponse.getTsspJsonResponse().getStatuscode())) {
                    Log.d("debug", "Login response object created by casting parsed json");
                    callback.onPharmacyListReceived(pharmacyListResponse);
                } else if ("010".equals(pharmacyListResponse.getTsspJsonResponse().getStatuscode())) {
                    callback.onPharmacyListFetchFailed();
                } else if ("020".equals(pharmacyListResponse.getTsspJsonResponse().getStatuscode())) {
                    callback.onPharmacyListFetchDbDown();
                } else if ("400".equals(pharmacyListResponse.getTsspJsonResponse().getStatuscode())) {
                    callback.onPharmacyListRequestInvalid();
                }
//                Log.d("debug", "Login response object created by casting parsed json");
//                callback.onPharmacyListReceived(pharmacyListResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {

                callback.onPharmacyListRequestTimeout();
                Log.d("TAG", "On pharmacy list request timeout");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                callback.onPharmacyListFetchDbDown();

            }
        };
        RequestHandle requestHandle = HttpUtil.httpCall(pharmacyListRequest, responseHandler);

        return requestHandle;
    }

    public List<PharmacyInfo> getPharmacyInfoList() {
        if (!hasPharmacyListResponse()) return null;

        return pharmacyListResponse.getTsspJsonResponse().getResponse().getGetPharmaciesResponse()
                .getPharmacyInfo();
    }
}
