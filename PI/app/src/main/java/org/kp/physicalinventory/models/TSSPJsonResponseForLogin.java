package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class TSSPJsonResponseForLogin {

    private String statuscode;
    private String statusdesc;
    private String transactionId;
    private ResponseForLogin response;

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusdesc() {
        return statusdesc;
    }

    public void setStatusdesc(String statusdesc) {
        this.statusdesc = statusdesc;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public ResponseForLogin getResponse() {
        return response;
    }

    public void setResponse(ResponseForLogin response) {
        this.response = response;
    }
}
