package org.kp.physicalinventory.core;

import android.util.Log;

import com.google.gson.Gson;

import org.kp.physicalinventory.models.CheckIfCountedResponse;
import org.kp.physicalinventory.models.LoginResponse;
import org.kp.physicalinventory.models.NDCListResponse;
import org.kp.physicalinventory.models.PharmacyListResponse;
import org.kp.physicalinventory.models.TSSPJsonResponseForCountUpdate;
import org.kp.physicalinventory.models.TSSPJsonResponseForNDCList;
import org.kp.physicalinventory.models.UpdateCountResponse;
import org.kp.physicalinventory.models.RackStatusUpdateResponse;
import org.kp.physicalinventory.models.RackStatusGetResponse;

/**
 * Created by is-4586 on 11/6/17.
 */

public class JsonParser {

    public static LoginResponse ParseJsonForLogin(String jsonString){
//        Log.d("debug", "Parsing Json for login");
        Gson gson = new Gson();
        return gson.fromJson(jsonString, LoginResponse.class);
    }

    public static PharmacyListResponse ParseJsonForPharmacyList(String jsonString){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, PharmacyListResponse.class);
    }

    public static NDCListResponse ParseJsonForNdcList(String jsonString){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, NDCListResponse.class);
    }

    public static TSSPJsonResponseForCountUpdate ParseJsonForCountUpdate(String jsonString){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, TSSPJsonResponseForCountUpdate.class);
    }

    public static RackStatusUpdateResponse ParseJsonForRackStatusUpdate(String jsonString){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, RackStatusUpdateResponse.class);
    }

    public static RackStatusGetResponse ParseJsonForRackStatusGet(String jsonString){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, RackStatusGetResponse.class);
    }

    public static CheckIfCountedResponse ParseJsonForCheckIfCounted(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, CheckIfCountedResponse.class);
    }
}
