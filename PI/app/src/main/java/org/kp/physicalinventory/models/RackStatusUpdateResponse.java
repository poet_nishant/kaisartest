package org.kp.physicalinventory.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusUpdateResponse {

    @SerializedName("TSSPJsonResponse")
    private TSSPJsonResponseForUpdateRack tsspJsonResponse;

    public TSSPJsonResponseForUpdateRack getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForUpdateRack tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }
}
