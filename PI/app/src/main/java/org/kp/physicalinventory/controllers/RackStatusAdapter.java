package org.kp.physicalinventory.controllers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import org.kp.physicalinventory.models.RackInfo;
import org.kp.physicalinventory.R;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusAdapter extends ArrayAdapter<RackInfo> {

    private RackCountCallback callback;
    private static final String TAG = RackStatusAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private final ViewBinderHelper binderHelper;
    private String username;

    public interface RackCountCallback {
        void onCompleteCountClicked(RackInfo rackInfo);
        void onPauseCountClicked(RackInfo rackInfo);
        void onDeleteCountClicked(RackInfo rackInfo);
        void onResumeCountClicked(RackInfo rackInfo);
        void onRackItemClicked(RackInfo rackInfo);
    }

    public RackStatusAdapter(Context context, ArrayList<RackInfo> racks, String username_selected, RackCountCallback callback) {
        super(context, R.layout.item_rack, racks);
        this.callback = callback;
        mInflater = LayoutInflater.from(context);
        binderHelper = new ViewBinderHelper();
        Log.d(TAG, "Inside rack status adapter" + username_selected);
        this.username = username_selected;
    }

    private Boolean isInProgressBySameUser() {
        Boolean isInProgress = false;
        if (RackStatusGetController.getInstance().getRackStatusGetResponse() != null) {
            Log.d(TAG, "is in progress by same user, check if rack response is not null");
            int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
            for (int i = 0; i < size; i++) {
                if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                    if (username.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                        isInProgress = true;
                    }
                }
            }
        }
        return isInProgress;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        // Get the data item for this position
        RackInfo rackInfo = getItem(position);

        Log.d(TAG,"index:" + position + " convertView" + convertView);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_rack, parent, false);
            holder = new ViewHolder();
            holder.rackNum = (TextView) convertView.findViewById(R.id.rack_num);
            holder.rackStatus = (TextView) convertView.findViewById(R.id.rack_status);
            holder.rackUsername = (TextView) convertView.findViewById(R.id.rack_username);
            holder.swipeLayout = (SwipeRevealLayout) convertView.findViewById(R.id.rack_item_view);
            holder.completeCountView = (RelativeLayout) convertView.findViewById(R.id.complete_count_view);
            holder.pauseCountView = (RelativeLayout) convertView.findViewById(R.id.pause_count_view);
            holder.resumeCountView = (RelativeLayout) convertView.findViewById(R.id.resume_count_view);
            holder.deleteCountView = (RelativeLayout) convertView.findViewById(R.id.delete_count_view);
            holder.rackItemSlideHint = (ImageButton) convertView.findViewById(R.id.rack_item_slide_hint);
            holder.rackItem = (RelativeLayout) convertView.findViewById(R.id.rack_item_slideable_view);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(rackInfo != null) {
            binderHelper.bind(holder.swipeLayout, rackInfo.getRackNo());
            holder.rackNum.setText(rackInfo.getRackNo());
            holder.rackUsername.setText(rackInfo.getCountedBy());
            holder.completeCountView.setOnClickListener(view -> callback.onCompleteCountClicked(getItem(position)));
            holder.pauseCountView.setOnClickListener(view -> callback.onPauseCountClicked(getItem(position)));
            holder.resumeCountView.setOnClickListener(view -> callback.onResumeCountClicked(getItem(position)));
            holder.deleteCountView.setOnClickListener(view -> callback.onDeleteCountClicked(getItem(position)));
            holder.rackItem.setOnClickListener(view -> callback.onRackItemClicked(getItem(position)));

            // Return the completed view to render on screen
            Log.d(TAG,"Rack Status:" + rackInfo.getRackStatus());
            Log.d(TAG, "Rack counted by" + rackInfo.getCountedBy());

            if("COMPLETED".equals(rackInfo.getRackStatus())) {
                Log.d(TAG, "boolean" + isInProgressBySameUser());
                if (isInProgressBySameUser()) {
                    Log.d(TAG, "in completed, check if in progress - check if in progress by same username");
                    holder.pauseCountView.setVisibility(View.GONE);
                    holder.completeCountView.setVisibility(View.GONE);
                    holder.deleteCountView.setVisibility(View.GONE);
                    holder.resumeCountView.setVisibility(View.GONE);
                    holder.rackItemSlideHint.setVisibility(View.INVISIBLE);
                    //holder.rackItemSlideHint.setOnClickListener(null);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_green));
                    holder.rackStatus.setText("Completed");

                }

                else{
                    Log.d(TAG, "in completed, else");
                    holder.pauseCountView.setVisibility(View.GONE);
                    holder.completeCountView.setVisibility(View.GONE);
                    holder.deleteCountView.setVisibility(View.GONE);
                    holder.resumeCountView.setVisibility(View.GONE);
                    holder.rackItemSlideHint.setVisibility(View.VISIBLE);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_green));
                    holder.rackStatus.setText("Completed");
                }
            }


            else if ("IN_PROGRESS".equals(rackInfo.getRackStatus())) {
                if (username.toLowerCase().equals(rackInfo.getCountedBy().toLowerCase())) {
                    holder.pauseCountView.setVisibility(View.VISIBLE);
                    holder.completeCountView.setVisibility(View.VISIBLE);
                    holder.deleteCountView.setVisibility(View.GONE);
                    holder.resumeCountView.setVisibility(View.GONE);
                    holder.rackItemSlideHint.setVisibility(View.VISIBLE);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_orange));
                    holder.rackStatus.setText("In progress");
                } else {
                    holder.rackItemSlideHint.setVisibility(View.INVISIBLE);
                    holder.rackItemSlideHint.setOnClickListener(null);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_orange));
                    holder.rackStatus.setText("In progress");
                    holder.swipeLayout.setLockDrag(true);
                }
            }

            else if ("PAUSED".equals(rackInfo.getRackStatus()) || "PAUSE".equals(rackInfo.getRackStatus())){
                if (RackStatusGetController.getInstance().getRackStatusGetResponse() != null) {
                    int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
                    if (isInProgressBySameUser()) {
                        Log.d(TAG, "paused, one rack in progress of same username as logged in");
                        holder.rackItemSlideHint.setVisibility(View.INVISIBLE);
                        holder.rackItemSlideHint.setOnClickListener(null);
                        holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_orange));
                        holder.rackStatus.setText("Paused");
                        holder.swipeLayout.setLockDrag(true);
                    } else {
                        holder.pauseCountView.setVisibility(View.GONE);
                        holder.completeCountView.setVisibility(View.VISIBLE);
                        holder.deleteCountView.setVisibility(View.GONE);
                        holder.resumeCountView.setVisibility(View.GONE);
                        holder.rackItemSlideHint.setVisibility(View.VISIBLE);
                        holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectange_orange));
                        holder.rackStatus.setText("Paused");

                    }
                }
            }

            else if ("NEW".equals(rackInfo.getRackStatus())){
                //int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
                //Log.d(TAG, "the size is" + size);
                if (isInProgressBySameUser()) {
                    holder.deleteCountView.setVisibility(View.GONE);
                    holder.completeCountView.setVisibility(View.GONE);
                    holder.resumeCountView.setVisibility(View.GONE);
                    holder.pauseCountView.setVisibility(View.GONE);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectangle_blue));
                    holder.rackStatus.setText("Not started");
                    holder.rackItemSlideHint.setVisibility(View.INVISIBLE);
                }
                else{
                    holder.deleteCountView.setVisibility(View.VISIBLE);
                    holder.completeCountView.setVisibility(View.GONE);
                    holder.resumeCountView.setVisibility(View.GONE);
                    holder.pauseCountView.setVisibility(View.GONE);
                    holder.rackStatus.setBackground(getContext().getDrawable(R.drawable.shape_rectangle_blue));
                    holder.rackStatus.setText("Not started");
                    holder.rackItemSlideHint.setVisibility(View.VISIBLE);
                }
            }
        }
        return convertView;
    }

    private static class ViewHolder {
        RelativeLayout rackItem;
        TextView rackNum;
        TextView rackStatus;
        TextView rackUsername;
        ImageButton rackItemSlideHint;
        RelativeLayout completeCountView;
        RelativeLayout pauseCountView;
        RelativeLayout deleteCountView;
        RelativeLayout resumeCountView;
        SwipeRevealLayout swipeLayout;
    }
}

