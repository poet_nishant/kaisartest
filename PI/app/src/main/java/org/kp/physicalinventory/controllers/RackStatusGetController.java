package org.kp.physicalinventory.controllers;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.RackStatusGetRequest;
import org.kp.physicalinventory.models.RackStatusGetResponse;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusGetController {
    private static RackStatusGetController instance = null;
    public static RackStatusGetController getInstance() {
        if(instance == null) {
            instance = new RackStatusGetController();
        }
        return instance;
    }

    private RackStatusGetController() {}

    public RackStatusGetResponse getRackStatusGetResponse() {
        return rackStatusGetResponse;
    }

    private RackStatusGetResponse rackStatusGetResponse;

    public void clearResponse(){
        rackStatusGetResponse = null;
        //Log.d("LoginController", "Clearing login response");
    }

    public interface RackStatusGetCallback {
        void onRackStatusGetPassed(RackStatusGetResponse rackStatusGetResponse);
        void onRackStatusGetFailed();
        void onRackStatusGetDbAccessFailed();
        void onRackStatusGetRequestInvalid();
        void onRackStatusGetRequestTimeout();
        void onRackStatusNotFound();
    }

    //public boolean hasRackStatusUpdateResponse() {
    //return rackStatusUpdateResponse != null;}

    public RequestHandle getRackStatus(String userid, String storeId, final RackStatusGetController.RackStatusGetCallback callback){

        Log.d("TAG", "Inside get rack status");
        final RackStatusGetRequest rackStatusGetRequest = new RackStatusGetRequest();
        rackStatusGetRequest.setDeviceid("");
        rackStatusGetRequest.setAppver(null);
        rackStatusGetRequest.setAppid(null);
        rackStatusGetRequest.setAction("getRackStatus");
        rackStatusGetRequest.setMode(null);
        rackStatusGetRequest.setRegion(null);
        rackStatusGetRequest.setStoreid("9000" + storeId);
        rackStatusGetRequest.setUserid(userid);
        rackStatusGetRequest.setTransactionid(null);
        rackStatusGetRequest.setTimeZone(TimeZoneController.getTimeZone());

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String httpResponseBody = response.toString();
                //String str = "{"statuscode":"000", "statusdesc":"Success", "transactionId":"d4afb36a-9b5e-48ee-b99f-0f73380bb430", "response":{"getRackStatusResponse":{"rackInfo":[{"phmNo":"01230","rackNo":"1a3we43", "rackStatus":"inProgress"}]}}";

                Log.d("debug", "The response body is: " + httpResponseBody);
                rackStatusGetResponse = (JsonParser.ParseJsonForRackStatusGet(httpResponseBody));
                if ("000".equals(rackStatusGetResponse.getTsspJsonResponse().getStatuscode())) {
                    Log.d("debug", "Rack status response object created by casting parsed json");
                    callback.onRackStatusGetPassed(rackStatusGetResponse);
                }
                else if("010".equals(rackStatusGetResponse.getTsspJsonResponse().getStatuscode())){
                    callback.onRackStatusNotFound();
                }
                else if("020".equals(rackStatusGetResponse.getTsspJsonResponse().getStatuscode())){
                    callback.onRackStatusGetDbAccessFailed();
                }
                else if("400".equals(rackStatusGetResponse.getTsspJsonResponse().getStatuscode())){
                    callback.onRackStatusGetRequestInvalid();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {
                if (t.getCause() instanceof ConnectTimeoutException) {
                    callback.onRackStatusGetRequestTimeout();
                    Log.d("TAG", "On pharmacy list request timeout");
                }
                else{
                    callback.onRackStatusGetFailed();
                }
            }
        };
        RequestHandle requestHandle = HttpUtil.httpCall(rackStatusGetRequest, responseHandler);

        return requestHandle;
    }
}
