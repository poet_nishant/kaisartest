package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class ResponseForPharmacyList {

    private GetPharmaciesResponse getPharmaciesResponse;

    public GetPharmaciesResponse getGetPharmaciesResponse() {
        return getPharmaciesResponse;
    }

    public void setGetPharmaciesResponse(GetPharmaciesResponse getPharmaciesResponse) {
        this.getPharmaciesResponse = getPharmaciesResponse;
    }
}
