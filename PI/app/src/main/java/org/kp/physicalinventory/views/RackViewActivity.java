package org.kp.physicalinventory.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestHandle;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.AlertDialogManager;
import org.kp.physicalinventory.controllers.ArrowViewController;
import org.kp.physicalinventory.controllers.LoginController;
import org.kp.physicalinventory.controllers.RackStatusAdapter;
import org.kp.physicalinventory.controllers.RackStatusAdapter.RackCountCallback;
import org.kp.physicalinventory.controllers.RackStatusGetController;
import org.kp.physicalinventory.controllers.RackStatusUpdateController;
import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.RackInfo;
import org.kp.physicalinventory.models.RackStatusGetResponse;
import org.kp.physicalinventory.models.RackStatusUpdateResponse;

import java.util.ArrayList;
import java.util.List;

public class RackViewActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        RackCountCallback,AlertDialogFragment.IAlertDialogCallback {

    private static final String TAG = MainActivity.class.getSimpleName();
    private FloatingActionButton addRack;
    private String rack_num_selected;
    ArrayList<RackInfo> rackInfoList;
    List<String> rackNumbers;
    RequestHandle requestHandle = null;
    private String storeID_selected;
    private String username_selected;
    private String storename_selected;
    private RackInfo mSelectedRackInfo;
    private String response;
    private RackStatusGetResponse rackStatusGetResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rackview);
        FloatingActionButton addRackButton = (FloatingActionButton) findViewById(R.id.add_rack);
        ArrowViewController arrow = (ArrowViewController) findViewById(R.id.arrow);
        ListView rackList = (ListView) findViewById(R.id.list_view_racks);
        TextView text = (TextView) findViewById(R.id.tapToStart);
        Intent i = getIntent();
        storeID_selected = LoginController.getInstance().getStoreNumber();
        username_selected = LoginController.getInstance().getUsername();
        storename_selected = LoginController.getInstance().getStoreName();
        setTitle(R.string.rack_view);
        if (isUserRoleTechnician()){
            Log.d(TAG, "user is technician, add rack button toggle visibility");
            addRackButton.setVisibility(View.GONE);
            arrow.setVisibility(View.GONE);
            text.setVisibility(View.GONE);
            rackList.setPadding(0,0,0,100);
        }
        else {
            arrow.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
            addRack = (FloatingActionButton) findViewById(R.id.add_rack);
            addRack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addRack();
                }
            });
        }

        TextView title_storename = (TextView) findViewById(R.id.title_pharmacy_name) ;
        title_storename.setText(storename_selected);

        NavigationView navigationHeader = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout =
                navigationHeader.inflateHeaderView(R.layout.nav_header_rack);
        TextView userNuid = (TextView) headerLayout.findViewById(R.id.user_nuid);
        TextView pharmacyDetails = (TextView) headerLayout.findViewById(R.id.pharmacy_details);
        userNuid.setText(username_selected);
        pharmacyDetails.setText(storename_selected);

      //response = "{\"TSSPJsonResponse\":{\"statuscode\":\"000\", \"statusdesc\":\"Success\", \"transactionId\":\"abcdef\", \"response\":{\"getRackStatusResponse\":{\"rackInfo\":[{\"phmNo\":\"03006\", \"rackNo\":\"102\", \"rackStatus\":\"IN_PROGRESS\", \"countedBy\":\"W081411\"}, {\"phmNo\":\"03006\", \"rackNo\":\"102\", \"rackStatus\":\"IN_PROGRESS\", \"countedBy\":\"W081412\"},{\"phmNo\":\"03006\", \"rackNo\":\"104\", \"rackStatus\":\"COMPLETED\", \"countedBy\":\"W081411\"}]}}}}";
      //rackStatusGetResponse = (JsonParser.ParseJsonForRackStatusGet(response));
      //RackStatusGetController.getInstance().setRackStatusGetResponse(rackStatusGetResponse);
      // getRackList();

        getRackListFromAPI();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RelativeLayout details = (RelativeLayout) findViewById(R.id.user_store_details) ;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private boolean isUserRoleTechnician(){
        if ("TECHNICIAN".equals(LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getUserRole(0))){
            return true;
        } else{
            return false;
        }
    }

    private void addRack() {
        if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
            int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
            Log.d(TAG, "The size is: " + size);
            for (int i = 0; i < size; i++) {
                Log.d(TAG, "Inside the for loop");
                if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                    if (username_selected.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {

                        LayoutInflater inflater = getLayoutInflater();
                        View promptsView = inflater.inflate(R.layout.rack_count_in_progress_dialog, null);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RackViewActivity.this);

                        alertDialogBuilder.setView(promptsView);
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setTitle("Section count in progress");
                        alertDialogBuilder.setPositiveButton("DISMISS", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        return;
                    }
                }
            }
        }

        LayoutInflater inflater = getLayoutInflater();
        View promptsView = inflater.inflate(R.layout.rack_num_input, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RackViewActivity.this);
        alertDialogBuilder.setTitle(R.string.dialog_enter_rack_number);
        alertDialogBuilder.setView(promptsView);
        final EditText input = (EditText) promptsView.findViewById(R.id.input);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String enteredRackNum = input.getText().toString();
                if ("".equals(enteredRackNum)) {
                    Log.d(TAG, "Null input");
                } else if (doesRackNumberAlreadyExist(enteredRackNum)) {
                    LayoutInflater inflater = getLayoutInflater();
                    View promptsView = inflater.inflate(R.layout
                            .rack_count_in_progress_dialog, null);
                    TextView text = (TextView) promptsView.findViewById(R.id.title);
                    text.setText("");
                    AlertDialogManager.rackAlreadyExist(RackViewActivity.this);

                } else {
                    rack_num_selected = input.getText().toString();
                    updateRackStatusForRack(rack_num_selected, "NEW");
                }
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private boolean doesRackNumberAlreadyExist(String enteredRackNum) {
        if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
            int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
            Log.d(TAG, "The size is: " + size);
            for (int i = 0; i < size; i++) {
                Log.d(TAG, "Inside the for loop");
                if (enteredRackNum.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().get(i).getRackNo().toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    void logoutUserCallback() {
        Intent intent = new Intent(RackViewActivity.this, MainActivity.class);
        intent.putExtra("IdleLogout",true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Log.d(TAG, "logout user call back");
        //Log.d(TAG, RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(0).getRackNo().toString());
        if(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
            Log.d(TAG, RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(0).getRackNo().toString());
            for (int i = 0; i < RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size(); i++) {
                if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                    Log.d(TAG, "If in progress");
                    if (username_selected.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                        Log.d(TAG, "if username has scanned in progress");
                        updateRackStatusForRack(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackNo(), "PAUSE");
                    }
                }
            }
        }
        LoginController.getInstance().clearLoginResponse();
        AppDatabase.destroyInstance(this);

        startActivity(intent);
    }

    public void getRackList(){
        displayRackList();
    }

    public void getRackListFromAPI(){
        //String json = "{\"statuscode\":\"000\", \"statusdesc\":\"Success\", \"transactionId\":\"d4afb36a-9b5e-48ee-b99f-0f73380bb430\", \"response\":{\"getRackStatusResponse\":{\"rackInfo\":[{\"phmNo\":\"01230\",\"rackNo\":\"1a3we43\", \"rackStatus\":\"inProgress\"}]}}";
        //Gson gson = new Gson();
        //gson.fromJson(json, RackStatusGetResponse.class);

        RackStatusGetController rackStatusGetController = RackStatusGetController.getInstance();

        RackStatusGetController.RackStatusGetCallback callback = new RackStatusGetController.RackStatusGetCallback() {
                @Override
                public void onRackStatusGetPassed(RackStatusGetResponse rackStatusGetResponse) {

                    // Handle scenario for status code ! = 000)
                    Log.d("debug", "Rack status successfull callback");
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("debug", "Before call to show success dialog");
                            //showSuccessDialog();
                            if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse() != null){
                                Log.d(TAG, "Rack status get response = null");
                                ArrowViewController arrow = (ArrowViewController) findViewById(R.id.arrow);
                                arrow.setVisibility(View.INVISIBLE);
                                TextView text = (TextView) findViewById(R.id.tapToStart);
                                text.setVisibility(View.INVISIBLE);
                            }
                                displayRackListFromAPIData();

                        }
                    });
                }

                @Override
                public void onRackStatusGetFailed() {
                    requestHandle = null;
                    Log.d(TAG, "on rack status get failed");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrowViewController arrow = (ArrowViewController) findViewById(R.id.arrow);
                            arrow.setVisibility(View.INVISIBLE);
                            TextView text = (TextView) findViewById(R.id.tapToStart);
                            text.setVisibility(View.INVISIBLE);
                            showFailureDialog();
                        }
                    });
                }

            @Override
            public void onRackStatusNotFound() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse() == null){
                            Log.d(TAG, "Rack status get response = null");
                            ArrowViewController arrow = (ArrowViewController) findViewById(R.id.arrow);
                            arrow.setVisibility(View.VISIBLE);
                            TextView text = (TextView) findViewById(R.id.tapToStart);
                            text.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            public void onRackStatusGetRequestTimeout() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //LoginController.getInstance().clearLoginResponse();
                        Log.d("TAG", "on login request timeout");
                        showFailureDialogForRequestTimeout();

                    }
                });
            }

                public void onRackStatusGetDbAccessFailed(){
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrowViewController arrow = (ArrowViewController) findViewById(R.id.arrow);
                            arrow.setVisibility(View.INVISIBLE);
                            TextView text = (TextView) findViewById(R.id.tapToStart);
                            text.setVisibility(View.INVISIBLE);
                            showFailureDialog();
                        }
                    });
                }

                public void onRackStatusGetRequestInvalid(){
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showFailureDialog();
                        }
                    });
                }

            };



            Log.d("debug", "Calling method login using ID and password");
            EditText username = (EditText)findViewById(R.id.username_pi);
            Log.d(TAG, "find view by id username");

            EditText password = (EditText)findViewById(R.id.password_pi) ;
            Log.d(TAG, "find view by id password");

            requestHandle = rackStatusGetController.getRackStatus(username_selected, storeID_selected,
                    callback);

    }

    public void displayRackList() {

        rackInfoList = rackStatusGetResponse.getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo();

        if (rackInfoList == null) {
            Log.d(TAG, "RackInfo list is null, exiting activity");
            Toast.makeText(getApplicationContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        rackNumbers = new ArrayList<>(rackInfoList.size());
        for (RackInfo r : rackInfoList) {
            rackNumbers.add(r.toString());
        }

        if (rackStatusGetResponse != null) {

            RackStatusAdapter adapter = new RackStatusAdapter(getApplicationContext(), rackInfoList, username_selected, this);
            ListView listView = (ListView) findViewById(R.id.list_view_racks);
            listView.setAdapter(adapter);
        }
    }

    public void displayRackListFromAPIData(){

        rackInfoList = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo();
        if (rackInfoList == null) {
            Log.d(TAG, "RackInfo list is null, exiting activity");
            Toast.makeText(getApplicationContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        rackNumbers = new ArrayList<>(rackInfoList.size());
        for (RackInfo r : rackInfoList) {
            rackNumbers.add(r.toString());
        }

        Log.d(TAG, "Before calling adapter, username" + username_selected);
        if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo() != null) {
            Log.d(TAG, "Inside display rack list, after checking if not null");
            RackStatusAdapter adapter = new RackStatusAdapter(getApplicationContext(), rackInfoList, username_selected, this);
            ListView listView = (ListView) findViewById(R.id.list_view_racks);
            listView.setAdapter(adapter);
        }
    }

    private void showFailureDialog(){
        AlertDialogManager.showUnableToConnectDialog(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(RackViewActivity.this, MainActivity.class);
            LoginController.getInstance().clearLoginResponse();
            AppDatabase.destroyInstance(this);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rack, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        if (id == R.id.nav_rackView) {
            // Handle the camera action
//            item.setIcon(R.drawable.symbols_icon_list);
//            Intent intent = new Intent(getApplicationContext(), RackViewActivity.class);
//            startActivity(intent);

            //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

        } else if (id == R.id.nav_logout) {


            Intent logout = new Intent(getApplicationContext(), MainActivity.class);
            Log.d(TAG, "logout user call back");

            if(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
                Log.d(TAG, RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(0).getRackNo().toString());
                for (int i = 0; i < RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size(); i++) {
                    if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                        Log.d(TAG, "If in progress");
                        if (username_selected.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                            Log.d(TAG, "if username has scanned in progress");
                            updateRackStatusForRack(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackNo(), "PAUSE");
                        }
                    }
                }
            }
            LoginController.getInstance().clearLoginResponse();
            AppDatabase.destroyInstance(this);
            startActivity(logout);
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCompleteCountClicked(RackInfo rackInfo) {
        mSelectedRackInfo = rackInfo;
        AlertDialogManager.showCompleteCountDialog(this, rackInfo.getRackNo());
    }

    @Override
    public void onPauseCountClicked(RackInfo rackInfo) {
        mSelectedRackInfo = rackInfo;
        AlertDialogManager.showPauseCountDialog(this);
    }

    @Override
    public void onResumeCountClicked(RackInfo rackInfo) {
        mSelectedRackInfo = rackInfo;
        AlertDialogManager.showResumeCountDialog(this, rackInfo.getRackNo());
    }

    @Override
    public void onDeleteCountClicked(RackInfo rackInfo) {
        mSelectedRackInfo = rackInfo;
        AlertDialogManager.showDeleteCountDialog(this, rackInfo.getRackNo());
    }

    private Boolean isInProgressBySameUser() {
        Boolean isInProgress = false;
        int size = RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size();
        for (int i = 0; i < size; i++) {
            if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                if (username_selected.toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                    isInProgress = true;
                }
            }
        }
        return isInProgress;

    }
    @Override
    public void onRackItemClicked(RackInfo rackInfo){
        mSelectedRackInfo = rackInfo;
        if (rackInfo.getRackStatus().equals("IN_PROGRESS")) {
            if (!rackInfo.getCountedBy().toLowerCase().equals(username_selected.toLowerCase())) {
                AlertDialogManager.showRackCountInProgressDialog2(this, rackInfo.getRackNo());
            }
            else {
                Intent intent = new Intent(RackViewActivity.this, ScanNdcActivity.class);
                intent.putExtra("rackNum", rackInfo.getRackNo());
//                intent.putExtra("username", username_selected);
//                intent.putExtra("storeId", storeID_selected);
                startActivity(intent);
            }
        }
        else if (rackInfo.getRackStatus().equals("PAUSE") || rackInfo.getRackStatus().equals("PAUSED")){
            if (isInProgressBySameUser()){
                AlertDialogManager.showRackCountInProgressDialog(this);
            }
            else {
                Log.d(TAG, "else loop of pause - before show resume count dialog");
                AlertDialogManager.showResumeCountDialog(this, rackInfo.getRackNo());

            }
        }
        else if (rackInfo.getRackStatus().equals("COMPLETED")){
            if (isInProgressBySameUser()){
                AlertDialogManager.showRackCountInProgressDialog(this);
            }
            else {
                AlertDialogManager.makeChangesForRackDialog(this, rackInfo.getRackNo());

            }
        }
        else if (rackInfo.getRackStatus().equals("NEW")){
            if (isInProgressBySameUser()){
                Log.d(TAG, "rack view activity, if in progress by same user");
                AlertDialogManager.showRackCountInProgressDialog(this);
            } else {
                Intent intent = new Intent(RackViewActivity.this, ScanNdcActivity.class);
                intent.putExtra("rackNum", rackInfo.getRackNo());
//                intent.putExtra("username", username_selected);
//                intent.putExtra("storeId", storeID_selected);
                startActivity(intent);
            }
        }
    }

    public void updateRackStatusForRack(String rackNum, String rackStatus) {
        RackStatusUpdateController rackStatusUpdateController = RackStatusUpdateController.getInstance();

        RackStatusUpdateController.RackStatusUpdateCallback callback = new RackStatusUpdateController.RackStatusUpdateCallback() {

            @Override
            public void onRackStatusUpdatePassed(RackStatusUpdateResponse rackStatusUpdateResponse) {

                // Handle scenario for status code ! = 000)
                Log.d("debug", "Rack status successfull callback");
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("debug", "Before call to show success dialog");
                        //showSuccessDialog();
                        //displayRackListFromAPIData();
                        RackStatusGetController.getInstance().clearResponse();
                        if ("NEW".equals(rackStatus)){
                            displayToastForRackSubmitted(rackNum);
                        }
                        getRackListFromAPI();
                    }
                });
            }

            @Override
            public void onRackStatusUpdateFailed() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateDbAccessFailed(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestInvalid(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestTimeout(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //LoginController.getInstance().clearLoginResponse();
                        Log.d("TAG", "on login request timeout");
                        showFailureDialogForRequestTimeout();

                    }
                });
            }
        };

        Log.d("debug", "Calling method login using ID and password");
        EditText username = (EditText)findViewById(R.id.username_pi);
        Log.d(TAG, "find view by id username");

        EditText password = (EditText)findViewById(R.id.password_pi) ;
        Log.d(TAG, "find view by id password");

        requestHandle = rackStatusUpdateController.updateRackStatus(rackNum, rackStatus, username_selected, storeID_selected,
                callback);
    }

    private void displayToastForRackSubmitted(String newRackNumber){
        Toast toast = Toast.makeText(this, "message", Toast.LENGTH_LONG);
        toast.setText("Section " + newRackNumber + " is submitted");
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    private void showFailureDialogForRequestTimeout() {
        AlertDialogManager.showUnableToConnectDialog2(this);
    }

    @Override
    public void onDialogPositiveClick(String dialogTag){
        switch (dialogTag)
        {
            case AlertDialogManager.RESUME_RACK_DIALOG_TAG:
                if(mSelectedRackInfo!=null){
                    Log.d(TAG, "On dialog positive click of resume rack dialog tag");
                    Intent intent = new Intent(RackViewActivity.this, ScanNdcActivity.class);
                    intent.putExtra("rackNum", mSelectedRackInfo.getRackNo());
//                    intent.putExtra("username", username_selected);
//                    intent.putExtra("storeId", storeID_selected);
                    startActivity(intent);
                }
                break;

            case AlertDialogManager.MAKE_CHANGES_RACK_DIALOG_TAG:
                if(mSelectedRackInfo!=null){
                    Log.d(TAG, "On dialog positive click of resume rack dialog tag");
                    Intent intent = new Intent(RackViewActivity.this, ScanNdcActivity.class);
                    intent.putExtra("rackNum", mSelectedRackInfo.getRackNo());
//                    intent.putExtra("username", username_selected);
//                    intent.putExtra("storeId", storeID_selected);
                    startActivity(intent);
                }
                break;

            case AlertDialogManager.COMPLETE_RACK_DIALOG_TAG:
                if(mSelectedRackInfo!=null) {
                    updateRackStatusForRack(mSelectedRackInfo.getRackNo(), "COMPLETED");
                }
                break;
        }
    }

    @Override
    public void onDialogNegativeClick(String dialogTag) {
        switch (dialogTag){
            case AlertDialogManager.UNABLE_TO_CONNECT_DIALOG_TAG:
                getRackListFromAPI();
                break;
            case AlertDialogManager.DELETE_RACK_DIALOG_TAG:
                if(mSelectedRackInfo!=null){
                    updateRackStatusForRack(mSelectedRackInfo.getRackNo(), "DELETE");
                }
                break;

            case AlertDialogManager.PAUSE_RACK_DIALOG_TAG:
                if(mSelectedRackInfo!=null){
                    updateRackStatusForRack(mSelectedRackInfo.getRackNo(), "PAUSE");
                }
                break;
        }
    }
}
