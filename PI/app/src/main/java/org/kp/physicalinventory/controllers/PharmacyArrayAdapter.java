package org.kp.physicalinventory.controllers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import org.kp.physicalinventory.R;
import org.kp.physicalinventory.models.PharmacyInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by is-4586 on 11/13/17.
 */

public class PharmacyArrayAdapter extends ArrayAdapter<PharmacyInfo> {
    private static final String TAG = PharmacyArrayAdapter.class.getSimpleName();
    private final Context mContext;
    private final ArrayList<PharmacyInfo> mPharmacies;
    private final ArrayList<PharmacyInfo> mPharmacies_All;
    private final ArrayList<PharmacyInfo> mPharmacies_Suggestion;

    public PharmacyArrayAdapter(Context context, List<PharmacyInfo> pharmacies) {
        super(context, 0, pharmacies);
        this.mContext = context;
        this.mPharmacies = new ArrayList<>(pharmacies);
        this.mPharmacies_All = new ArrayList<>(pharmacies);
        this.mPharmacies_Suggestion = new ArrayList<>();
    }

    public int getCount() {
        return mPharmacies.size();
    }

    public PharmacyInfo getItem(int position) {
        return mPharmacies.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView position:" + position);
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.simple_text_view, parent, false);
            }
            PharmacyInfo pharmacy = getItem(position);
            TextView name = (TextView) convertView.findViewById(R.id.textview);
            name.setText(pharmacy.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                Log.d(TAG, "convertResultToString " + resultValue.toString());
                return resultValue.toString();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.d(TAG, "performFiltering " + constraint);
                if (constraint != null) {
                    mPharmacies_Suggestion.clear();
                    mPharmacies_Suggestion.addAll(AutoSuggestController.startsWith(constraint.toString()));
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mPharmacies_Suggestion;
                    filterResults.count = mPharmacies_Suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    Log.d(TAG, "publishResults numOfResults:" + results.count);
                } else {
                    Log.d(TAG, "publishResults with null results");
                }
                mPharmacies.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                    List<?> result = (List<?>) results.values;
                    for (Object object : result) {
                        if (object instanceof PharmacyInfo) {
                            mPharmacies.add((PharmacyInfo) object);
                        }
                    }
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mPharmacies.addAll(mPharmacies_All);
                }
                notifyDataSetChanged();
            }
        };
    }
}
