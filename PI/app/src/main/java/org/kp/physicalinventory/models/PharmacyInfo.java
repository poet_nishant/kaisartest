package org.kp.physicalinventory.models;
/**
 * Created by is-4586 on 11/2/17.
 */

public class PharmacyInfo {

    private String phmNo;
    private String phmName;
    private String phmCity;
    private String phmState;
    private String phmZip;
    private String phmAddress;

    public String getPhmNo() {
        return phmNo;
    }

    public void setPhmNo(String phmNo) {
        this.phmNo = phmNo;
    }

    public String getPhmName() {
        return phmName;
    }

    public void setPhmName(String phmName) {
        this.phmName = phmName;
    }

    public String getPhmCity() {
        return phmCity;
    }

    public void setPhmCity(String phmCity) {
        this.phmCity = phmCity;
    }

    public String getPhmState() {
        return phmState;
    }

    public void setPhmState(String phmState) {
        this.phmState = phmState;
    }

    public String getPhmZip() {
        return phmZip;
    }

    public void setPhmZip(String phmZip) {
        this.phmZip = phmZip;
    }

    public String getPhmAddress() {
        return phmAddress;
    }

    public void setPhmAddress(String phmAddress) {
        this.phmAddress = phmAddress;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(phmName).append(" (#").append(phmNo).append(")\n")
                .append(phmAddress).append(", ").append(phmCity).append(", ").append(phmState)
                .append(", ").append(phmZip);
        return sb.toString();
    }
}
