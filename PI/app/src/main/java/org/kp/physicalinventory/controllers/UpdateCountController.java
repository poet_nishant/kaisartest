package org.kp.physicalinventory.controllers;

/**
 * Created by is-4586 on 11/2/17.
 */

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.TSSPJsonResponseForCountUpdate;
import org.kp.physicalinventory.models.UpdateCountRequest;
import org.kp.physicalinventory.models.UpdateCountResponse;

import cz.msebera.android.httpclient.Header;

public class UpdateCountController {

    private static UpdateCountController instance = null;
    public static UpdateCountController getInstance() {
        if(instance == null) {
            instance = new UpdateCountController();
        }
        return instance;
    }

    private UpdateCountController() {}

    private TSSPJsonResponseForCountUpdate updateCountResponse;


    public interface UpdateCountCallback {
        void onUpdateCountPassed(TSSPJsonResponseForCountUpdate updateCountResponse);
        void onUpdateCountFailed();
        void onUpdateCountRequestTimeout();
        void onNdcNotFound();
    }


    public boolean hasUpdateCountResponse() {
        return updateCountResponse != null;
    }

    public RequestHandle updateCountForNdc(Boolean checkifManualEntry, String storeId, String username, String ndc, String drugName, Float wholePack,
                                           List<Float> partialPack, Float totalQty, String rackNo,
                                           final UpdateCountCallback callback){

        final UpdateCountRequest updateCountRequest = new UpdateCountRequest();

        if (checkifManualEntry){
            updateCountRequest.updatePINDCRequest().setDrugName(drugName);
        }
        updateCountRequest.setDeviceid("");
        updateCountRequest.setAppver(null);
        updateCountRequest.setAppid(null);
        updateCountRequest.setAction("updatePINDC");
        updateCountRequest.setMode("PROD");
        updateCountRequest.setRegion(LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getRegion(0));
        updateCountRequest.setStoreid("9000" + storeId);
        updateCountRequest.setUserid(username);
        updateCountRequest.setTransactionid(null);
        updateCountRequest.setTimeZone(TimeZoneController.getTimeZone());
        updateCountRequest.updatePINDCRequest().setNdc(ndc);
        updateCountRequest.updatePINDCRequest().setWholePack(wholePack);
        updateCountRequest.updatePINDCRequest().setPartialPack(partialPack);
        updateCountRequest.updatePINDCRequest().setTotalQty(totalQty);
        updateCountRequest.updatePINDCRequest().setRackNo(rackNo);

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String httpResponseBody = response.toString();
                updateCountResponse = JsonParser.ParseJsonForCountUpdate(httpResponseBody);
                Log.d("debug", "The response body is: " + httpResponseBody);
                callback.onUpdateCountPassed(updateCountResponse);
                if (updateCountResponse.getResponse() !=null) {
                    if ("COMPLETED".equals(updateCountResponse.getResponse().getUpdatePINDCResponse())) {
                        callback.onUpdateCountPassed(updateCountResponse);
                    }
                }
                else if ("Ndc Not Found.".equals(updateCountResponse.getStatusdesc())) {
                    callback.onNdcNotFound();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {


                Log.d("TAG", "On pharmacy list request timeout");
                callback.onUpdateCountRequestTimeout();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("TAG", "on failure of ndc list get");
                callback.onUpdateCountFailed();

            }
        };
        RequestHandle requestHandle = HttpUtil.httpCall(updateCountRequest, responseHandler);

        return requestHandle;
    }

}





