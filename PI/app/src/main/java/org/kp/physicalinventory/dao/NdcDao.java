package org.kp.physicalinventory.dao;

/**
 * Created by is-4586 on 11/28/17.
 */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.NDC;

import java.util.List;

@Dao
public interface NdcDao {

    @Query("SELECT * FROM NDC")
    List<NDC> getAll();

    @Query("SELECT * FROM ndc where barcode LIKE  :barcode")
    NDC findByBarcode(String barcode);

    @Query("SELECT * FROM ndc where ndc LIKE :ndc")
    NDC findByNdc(String ndc);

//    @Query("SELECT COUNT(*) from user")
//    int countUsers();

    @Insert
    void insertAll(NDC... ndc);

    @Insert
    void insertList(List<NDC> list);

    @Query("DELETE FROM ndc")
    public void deleteAll();

}
