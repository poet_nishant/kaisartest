package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class NdcListRequest {

    private String deviceid;
    private String appver;
    private String appid;
    private String action;
    private String mode;
    private String region;
    private String storeid;
    private String userid;
    private String transactionid;
    private String timeZone;
    private AssignedNdcRequest assignedNDCRequest;

    public NdcListRequest() {
        assignedNDCRequest = new AssignedNdcRequest();
    }

    public AssignedNdcRequest getAssignedNdcRequest() {
        return assignedNDCRequest;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setAppver(String appver) {
        this.appver = appver;
    }

    public String getAppver() {
        return appver;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }


}
