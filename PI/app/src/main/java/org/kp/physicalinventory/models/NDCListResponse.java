package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/30/17.
 */

public class NDCListResponse {

    private TSSPJsonResponseForNDCList TSSPJsonResponse;

    public TSSPJsonResponseForNDCList getTSSPJsonResponse() {
        return TSSPJsonResponse;
    }

    public void setTSSPJsonResponse(TSSPJsonResponseForNDCList TSSPJsonResponse) {
        this.TSSPJsonResponse = TSSPJsonResponse;
    }
}
