package org.kp.physicalinventory.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.views.AlertDialogFragment;
import org.kp.physicalinventory.views.MainActivity;

public class AlertDialogManager {

    public static final String UNABLE_TO_CONNECT_DIALOG_TAG = "try again";
    public static final String ALERT_DIALOG_TAG = "alert";
    public static final String ALERT_DIALOG_DISMISS_TAG = "alert_dismiss";
    public static final String DELETE_RACK_DIALOG_TAG = "delete rack";
    public static final String RESUME_RACK_DIALOG_TAG = "resume rack";
    public static final String PAUSE_RACK_DIALOG_TAG = "pause rack";
    public static final String COMPLETE_RACK_DIALOG_TAG = "complete rack";
    public static final String MAKE_CHANGES_RACK_DIALOG_TAG = "make changes";


    public static void showAlertDialogWithOK(FragmentActivity activity, String title, String text) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle, title);
        bundle.putString(AlertDialogFragment.dialogText, text);
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                 activity.getResources().getString(R.string.dialog_ok));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), ALERT_DIALOG_TAG);
    }

    public static void showAlertDialogWithDismiss(FragmentActivity activity, String title, String text) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle, title);
        bundle.putString(AlertDialogFragment.dialogText, text);
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_dismiss));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), ALERT_DIALOG_DISMISS_TAG);
    }

    public static void showUnableToConnectDialog(FragmentActivity activity) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogText,
                activity.getResources().getString(R.string.dialog_unable_to_connect_text));
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_close));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_try_again));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), UNABLE_TO_CONNECT_DIALOG_TAG);

    }

    public static void showUnableToConnectDialog2(FragmentActivity activity) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogText,
                activity.getResources().getString(R.string.dialog_unable_to_connect_text2));
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_close));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_try_again));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), UNABLE_TO_CONNECT_DIALOG_TAG);

    }

    public static void showInvalidRequestDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                activity.getResources().getString(R.string.dialog_failure),
                activity.getResources().getString(R.string.dialog_invalid_request_text));
    }


    public static void showUserUnauthorizedDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
               "",
                activity.getResources().getString(R.string.dialog_user_unauthorized));
    }

    public static void showUserNotFoundDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                "",
                activity.getResources().getString(R.string.dialog_user_not_found));
    }

    public static void showAccountLockedDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                "",
                activity.getResources().getString(R.string.dialog_account_locked));
    }

    public static void showRackCountInProgressDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                activity.getResources().getString(R.string.dialog_rack_count_inprogress_title),
                activity.getResources().getString(R.string.dialog_rack_count_inprogress_text));
    }

    public static void makeChangesForRackDialog(FragmentActivity activity, String rackNo){
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_make_changes_rack_title));
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_make_changes_title));
        bundle.putString(AlertDialogFragment.dialogText, "Are you sure you want to make changes to section " + rackNo + "?\n\nThis action will change the status of the section back to \"In progress\"");
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_make_changes));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_cancel));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), MAKE_CHANGES_RACK_DIALOG_TAG);

    }

    public static void showRackCountInProgressDialog2(FragmentActivity activity, String rackNo) {
        showAlertDialogWithDismiss(activity,
                activity.getResources().getString(R.string.dialog_rack_count_inprogress_title), "Section " + rackNo + " is in process of counting by another user. Please select a different rack or use the + button to start a new rack count");

    }

    public static void showDeleteCountDialog(FragmentActivity activity, String rackNo) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_delete_rack_title));
        bundle.putString(AlertDialogFragment.dialogText,
                "Are you sure you want to DELETE section " + rackNo + " ?");
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_cancel));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_delete_rack));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), DELETE_RACK_DIALOG_TAG);
    }

    public static void showResumeCountDialog(FragmentActivity activity, String rackNo) {
        Log.d("TAG", "Show resume count dialog in alert dialog manager");
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_resume_count_title));
        bundle.putString(AlertDialogFragment.dialogText, "Are you sure you want to resume count on section " + rackNo + "?\n\nThis action will change the status of the section back to \"In progress\"");
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_resume_rack));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_cancel));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), RESUME_RACK_DIALOG_TAG);
    }

    public static void showPauseCountDialog(FragmentActivity activity) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_pause_rack_title));
        bundle.putString(AlertDialogFragment.dialogText,
                activity.getResources().getString(R.string.dialog_pause_rack_text));
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_cancel));
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_pause_rack));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), PAUSE_RACK_DIALOG_TAG);
    }

    public static void showCompleteCountDialog(FragmentActivity activity, String rackNo) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AlertDialogFragment.dialogTitle,
                activity.getResources().getString(R.string.dialog_complete_rack_title));
        bundle.putString(AlertDialogFragment.dialogText,
                "Are you sure you want to COMPLETE count for section " + rackNo + "?\n\nOnce a section is complete, changes can be made by tapping on the section row");
        bundle.putString(AlertDialogFragment.dialogNegativeBtnText,
                activity.getResources().getString(R.string.dialog_cancel));
        bundle.putString(AlertDialogFragment.dialogPositiveBtnText,
                activity.getResources().getString(R.string.dialog_complete_rack));
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(), COMPLETE_RACK_DIALOG_TAG);
    }

    public static void rackAlreadyExist(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                "",
                activity.getResources().getString(R.string.dialog_rack_already_exists));
    }

    public static void showIdleLogoutDialog(FragmentActivity activity) {
        showAlertDialogWithOK(activity,
                "",
                activity.getResources().getString(R.string.dialog_idle_logout_message));
    }

    public static void showAlertDialogForRestrictedAccess(FragmentActivity activity){
        showAlertDialogWithDismiss(activity,
                activity.getString(R.string.ndc_access_restricted_dialog_title),
                activity.getString(R.string.ndc_access_restricted_dialog_body));
    }

    public static void showDialogForBarcodeNotFoundInList(FragmentActivity activity){
        showAlertDialogWithDismiss(activity,
                activity.getString(R.string.ndc_not_found_dialog_title),
                activity.getString(R.string.ndc_not_found_dialog_body));
    }

    public static void showDialogForAlreadyCounted(FragmentActivity fragmentActivity) {
        showAlertDialogWithDismiss(fragmentActivity,
                fragmentActivity.getString(R.string.ndc_already_counted_dialog_title),
                fragmentActivity.getString(R.string.ndc_already_counted_dialog_body));
    }

    public static void showFailureDialog(Context context, Class clazz) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage(R.string.unable_to_reach_server);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setPositiveButton(R.string.try_again, (dialogInterface, i) -> {
            Intent intent = new Intent(context, clazz);
            context.startActivity(intent);
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(600, 400);
    }

    public static void showFailureDialogForRequestTimeout(Context context, Class clazz) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage(R.string.unable_to_connect_dialog_body);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setNegativeButton(R.string.try_again, (dialogInterface, i) -> {
            Intent intent = new Intent(context, clazz);
            context.startActivity(intent);
        });
    }

}