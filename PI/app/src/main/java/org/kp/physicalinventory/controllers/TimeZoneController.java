package org.kp.physicalinventory.controllers;

import android.util.Log;

import java.util.TimeZone;

/**
 * Created by is-4586 on 11/27/17.
 */

public class TimeZoneController {

    public static String getTimeZone() {

        Log.d("TAG", TimeZone.getDefault().getDisplayName());
        return TimeZone.getDefault().getID();

    }
}
