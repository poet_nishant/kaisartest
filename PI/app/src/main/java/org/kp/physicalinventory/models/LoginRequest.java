package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/1/17.
 */

public class LoginRequest {
    private String deviceid;
    private String appver;
    private String appid;
    private String action;
    private String mode;
    private String region;
    private String storeid;
    private String userid;
    private String password;
    private String transactionid;

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setAppver(String appver) {
        this.appver = appver;
    }

    public String getAppver() {
        return appver;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getTransactionid() {
        return transactionid;
    }

}






