package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class ResponseForNDCList {
    private GetPINDCListResponse getPINDCListResponse;

    public GetPINDCListResponse getGetPINDCListResponse() {
        return getPINDCListResponse;
    }

    public void setGetPINDCListResponse(GetPINDCListResponse getPINDCListResponse) {
        this.getPINDCListResponse = getPINDCListResponse;
    }
}

