package org.kp.physicalinventory.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by is-4586 on 11/2/17.
 */

public class CurrentDateController {

    public static String GetDate(){
        Date now = new Date();
        String format = new SimpleDateFormat("EEE, d MMM yyyy", Locale.ENGLISH).format(now);
        return format;
    }
}
