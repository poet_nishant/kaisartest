package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 1/5/18.
 */

public class CheckIfCountedResponseWrapper {
    private  ResponseForCheckIfCounted checkCountedPINDCResponse;

    public ResponseForCheckIfCounted getCheckCountedPINDCResponse() {
        return checkCountedPINDCResponse;
    }

    public void setCheckCountedPINDCResponse(ResponseForCheckIfCounted checkCountedPINDCResponse) {
        this.checkCountedPINDCResponse = checkCountedPINDCResponse;
    }
}
