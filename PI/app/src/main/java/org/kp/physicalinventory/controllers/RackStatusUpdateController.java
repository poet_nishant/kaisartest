package org.kp.physicalinventory.controllers;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.RackStatusUpdateRequest;
import org.kp.physicalinventory.models.RackStatusUpdateResponse;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusUpdateController {

    private static RackStatusUpdateController instance = null;

    public static RackStatusUpdateController getInstance() {
        if (instance == null) {
            instance = new RackStatusUpdateController();
        }
        return instance;
    }

    private RackStatusUpdateController() {
    }

    //private RackStatusUpdateResponse rackStatusUpdateResponse;

    //public LoginRequest getLoginRequest() {
    //  return loginRequest;
    // }

    public interface RackStatusUpdateCallback {
        void onRackStatusUpdatePassed(RackStatusUpdateResponse rackStatusResponse);

        void onRackStatusUpdateFailed();

        void onRackStatusUpdateDbAccessFailed();

        void onRackStatusUpdateRequestInvalid();

        void onRackStatusUpdateRequestTimeout();
    }

    //public boolean hasRackStatusUpdateResponse() {
    //return rackStatusUpdateResponse != null;}

    public RequestHandle updateRackStatus(String rackNo, String rackStatus, String userid, String storeId, final RackStatusUpdateController.RackStatusUpdateCallback callback) {

        final RackStatusUpdateRequest rackStatusUpdateRequest = new RackStatusUpdateRequest();

        Log.d("debug", "Login request instance");

        rackStatusUpdateRequest.setDeviceid("");
        rackStatusUpdateRequest.setAppver(null);
        rackStatusUpdateRequest.setAppid(null);
        rackStatusUpdateRequest.setAction("updateRackStatus");
        rackStatusUpdateRequest.setMode("PROD");
        rackStatusUpdateRequest.setRegion(LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getRegion(0));
        rackStatusUpdateRequest.setStoreid("9000" + storeId);
        rackStatusUpdateRequest.setUserid(userid);
        rackStatusUpdateRequest.setTransactionid(null);
        rackStatusUpdateRequest.setTimeZone(TimeZoneController.getTimeZone());
        rackStatusUpdateRequest.getUpdateRackStatusRequest().setRackNo(rackNo);
        rackStatusUpdateRequest.getUpdateRackStatusRequest().setRackStatus(rackStatus);

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String httpResponseBody = response.toString();
                Log.d("debug", "The response body is: " + httpResponseBody);
                RackStatusUpdateResponse rackStatusUpdateResponse = (JsonParser.ParseJsonForRackStatusUpdate(httpResponseBody));
                if ("000".equals(rackStatusUpdateResponse.getTsspJsonResponse().getStatuscode())) {
                    Log.d("debug", "Login response object created by casting parsed json");
                    callback.onRackStatusUpdatePassed(rackStatusUpdateResponse);
                } else if ("020".equals(rackStatusUpdateResponse.getTsspJsonResponse().getStatuscode())) {
                    callback.onRackStatusUpdateDbAccessFailed();
                } else if ("400".equals(rackStatusUpdateResponse.getTsspJsonResponse().getStatuscode())) {
                    callback.onRackStatusUpdateRequestInvalid();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {
                if (t.getCause() instanceof ConnectTimeoutException) {
                    callback.onRackStatusUpdateRequestTimeout();
                    Log.d("TAG", "On pharmacy list request timeout");
                } else {
                    callback.onRackStatusUpdateFailed();
                }
            }
        };
        RequestHandle requestHandle = HttpUtil.httpCall(rackStatusUpdateRequest, responseHandler);

        return requestHandle;
    }
}
