package org.kp.physicalinventory.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.PharmacyArrayAdapter;
import org.kp.physicalinventory.controllers.PharmacyListController;
import org.kp.physicalinventory.models.PharmacyInfo;
import java.util.ArrayList;
import java.util.List;

public class LocationSelectorActivity extends AppCompatActivity {

    private static final String TAG = LocationSelectorActivity.class.getSimpleName();
    AutoCompleteTextView locationSelector;
    List<PharmacyInfo> pharmacyInfoList;
    List<String> pharmacyNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selector);


        locationSelector = (AutoCompleteTextView) findViewById(R.id.location_selector);
        //locationSelector.setThreshold(1);
        pharmacyInfoList = PharmacyListController.getInstance().getPharmacyInfoList();
        if (pharmacyInfoList == null) {
            Log.d(TAG, "Pharmacy list is null, exiting activity");
            Toast.makeText(getApplicationContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }


        pharmacyNames = new ArrayList<>(pharmacyInfoList.size());
        for (PharmacyInfo pi : pharmacyInfoList) {
            pharmacyNames.add(pi.toString());
        }


        PharmacyArrayAdapter adapter = new PharmacyArrayAdapter(this, pharmacyInfoList);
        locationSelector.setAdapter(adapter);
        locationSelector.setThreshold(1);//start searching from 1 character
        locationSelector.setAdapter(adapter);//set the adapter for displaying country name list
        locationSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                locationSelector.showDropDown();
            }
        });

        locationSelector.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PharmacyInfo selectedPharmacy  = (PharmacyInfo) adapterView.getItemAtPosition(i);
                Intent result = new Intent();
                result.putExtra(MainActivity.SELECTED_STORE_ID, selectedPharmacy.getPhmNo());
                result.putExtra(MainActivity.SELECTED_LOCATION_NAME, selectedPharmacy.getPhmName());
                setResult(Activity.RESULT_OK, result);
                finish();
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG, "On resume of locator selector activity");
    }

    protected void onPause(){
        super.onPause();
        Log.d(TAG, "On pause of locator selector activity");
    }

    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "On destroy of locator selector activity");
    }
}

