package org.kp.physicalinventory.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.RequestHandle;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.AlertDialogManager;
import org.kp.physicalinventory.controllers.LoginController;
import org.kp.physicalinventory.controllers.RackStatusGetController;
import org.kp.physicalinventory.controllers.RackStatusUpdateController;
import org.kp.physicalinventory.controllers.UpdateCountController;
import org.kp.physicalinventory.core.TextUtility;
import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.NDC;
import org.kp.physicalinventory.models.RackStatusUpdateResponse;
import org.kp.physicalinventory.models.ResponseForCheckIfCounted;
import org.kp.physicalinventory.models.TSSPJsonResponseForCountUpdate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//import static org.kp.physicalinventory.views.ScanNdcActivity.CHECK_IF_COUNTED_RESPONSE;


public class NDCUpdateCountActivity extends BaseActivity implements CountView.ICountChange {
    public static final String SELECTED_NDC_EXTRA = "selectedNdc";
    public static final String RACK_NUM_EXTRA = "rackNum";
    public static final String CHECK_IF_COUNTED_RESPONSE = "checkIfCountedResponse";
    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.ndc_update_count_name)
    TextView mName;
    @BindView(R.id.ndc_update_count_schedule)
    TextView mSchedule;
    @BindView(R.id.ndc_update_count_manufacturer)
    TextView mManufacturer;
    @BindView(R.id.ndc_update_count_pack_size)
    TextView mPackSize;
    @BindView(R.id.ndc_update_count_ndc)
    TextView mNDC;
    @BindView(R.id.ndc_update_count_gpi)
    TextView mGpi;
    @BindView(R.id.ndc_update_count_total)
    TextView mTotal;
    @BindView(R.id.ndc_update_count_total_label)
    TextView mTotalLabel;
    @BindView(R.id.ndc_update_count_fullpack)
    TextView mFullPack;
    @BindView(R.id.ndc_update_count_partialpack)
    TextView mPartialPack;
    @BindView(R.id.ndc_update_count_fullpack_layout)
    CountView mFullPackCount;
    @BindView(R.id.ndc_update_count_partialpack_layout)
    CountView mPartialPackCount;
    @BindView(R.id.ndc_update_count_add_row_layout)
    LinearLayout mAddRowLayout;
    @BindView(R.id.ndc_update_count_add_row_scroll)
    ScrollView mAddRowScrollView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.ndc_update_count_add_row)
    TextView mAddPartial;
    private HashMap<View, Float> viewValueMap;
    private String mMeasurementUnit;
    private float mPackSizeCount;
    private int mPacksPerContainer;
    private float wholePacks;
    private float totalValue;
    private List<Float> partialPacks;
    private String rackNum;
    private String drug_name_manual;
    RequestHandle requestHandle = null;
    String ndcSelected;
    String uom;
    Boolean isNdcEntryManual = false;
    Boolean alreadyCounted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ndc_update_count);
        ButterKnife.bind(this);
        viewValueMap = new HashMap<>();
        partialPacks = new ArrayList<>();
        setUpViews();
        addValues();
    }

    private void setUpViews() {
        mFullPackCount.addCountChangeListener(this, true);
        mPartialPackCount.addCountChangeListener(this, false);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent i = new Intent(this, ScanNdcActivityTemp.class);
                i.putExtra("rackNum", rackNum);
                i.putExtra("fromNDCUpdateCountActivity", true);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addValues() {
        Gson gson = new Gson();
        Intent i = getIntent();
        rackNum = i.getStringExtra("rackNum");
        Bundle bundle = i.getExtras();

        if (bundle != null && bundle.getBoolean("DrugnameManualEntry")) {
            drug_name_manual = i.getStringExtra("selectedDrugName");
            ndcSelected = i.getStringExtra("selectedNdc");
            mName.append(getBoldAndBigSizeText(drug_name_manual));
            mNDC.append(getBigSizeText(ndcSelected));
            mMeasurementUnit = "Units";
            setTotalCountTxt(-1);
            isNdcEntryManual = true;
            mFullPackCount.setInputTypeToNumber();
            mPartialPackCount.setInputTypeToFloat();

            if (bundle != null && bundle.getBoolean("AlreadyCounted")) {
                Log.d(TAG, "In Already Counted.. populating activity with pack data");
                ResponseForCheckIfCounted response = gson.fromJson(i.getStringExtra(CHECK_IF_COUNTED_RESPONSE), ResponseForCheckIfCounted.class);
                Log.d(TAG, "setting whole pack pack");
                float wholePackToDisplay = response.getWholePack();
                wholePackToDisplay = Math.round(wholePackToDisplay);
                mFullPackCount.setCount(wholePackToDisplay);
                List<Float> partialPackResp = response.getPartialPackQty();
                if (partialPackResp.size() > 0) {
                    Log.d(TAG, "setting first partial pack");
                    mPartialPackCount.setCount(partialPackResp.get(0));
                }

                if (partialPackResp.size() > 1) {
                    for (int j = 1; j < partialPackResp.size(); j++) {
                        addPartialPackCountView(partialPackResp.get(j));
                    }
                }
            }

        } else {

            NDC ndc = gson.fromJson(i.getStringExtra(ScanNdcActivity.SELECTED_NDC_EXTRA), NDC.class);
            if (ndc != null) {
                String drug_name = ndc.getDrugName();
                setTitle(drug_name);
                ndcSelected = ndc.getNdc();
                drug_name_manual = "";
                uom = ndc.getUom();
                Log.d(TAG, "Inside add values, UOM is " + uom.toLowerCase());
                if (uom.toLowerCase().contains("ml")) {
                    Log.d(TAG, "Inside add values, check if input type is ml");
                    mFullPackCount.setInputTypeToNumber();
                    mPartialPackCount.setInputTypeToFloat();
                } else {
                    mFullPackCount.setInputTypeToNumber();
                    mPartialPackCount.setInputTypeToNumber();
                }
                mName.append(getBoldAndBigSizeText(drug_name));
                mSchedule.append(getBoldAndBigSizeText(ndc.getSchedule()));
                mManufacturer.append(getBigSizeText(ndc.getManufacturer()));
                mNDC.append(getBigSizeText(ndc.getNdc()));
                mGpi.append(getBigSizeText(ndc.getGpi()));
                mPackSizeCount = ndc.getPack();
                mPacksPerContainer = ndc.getPacksPerContainer();
                mPackSize.append(getBigSizeText(TextUtility.floatToStr(mPackSizeCount)));
                mMeasurementUnit = ndc.getUom();
                setTotalCountTxt(-1);

                if (bundle != null && bundle.getBoolean("AlreadyCounted")) {
                    Log.d(TAG, "In Already Counted.. populating activity with pack data");
                    ResponseForCheckIfCounted response = gson.fromJson(i.getStringExtra(CHECK_IF_COUNTED_RESPONSE), ResponseForCheckIfCounted.class);
                    Log.d(TAG, "setting whole pack pack");
                    float wholePackToDisplay = response.getWholePack() / (ndc.getPacksPerContainer() * ndc.getPack());
                    wholePackToDisplay = Math.round(wholePackToDisplay);
                    mFullPackCount.setCount(wholePackToDisplay);
                    List<Float> partialPackResp = response.getPartialPackQty();
                    if (partialPackResp.size() > 0 && partialPackResp.get(0)>0.0f) {
                        Log.d(TAG, "setting first partial pack");
                        mPartialPackCount.setCount(partialPackResp.get(0));
                    }

                    if (partialPackResp.size() > 1) {
                        for (int j = 1; j < partialPackResp.size(); j++) {
                            if(partialPackResp.get(j)>0.0f)
                            addPartialPackCountView(partialPackResp.get(j));
                        }
                    }
                }
            }
        }
    }

    private void setTotalCountTxt(float count) {
        if (count < 0) {
            mTotal.setText(TextUtils.concat(" ", mMeasurementUnit));
        } else {
            Spannable str = TextUtility.makeBold(TextUtility.floatToStr(count));
            // mTotal.setText(TextUtils.concat(str, " ", mMeasurementUnit));
            mTotal.setText(TextUtils.concat(str, " "));
        }
    }

    private Spannable getPackSizeText(int packSize) {
        return setNormalStyleText(getString(R.string.ndc_update_count_pack_size) + packSize);
    }

    private Spannable setNormalStyleText(String str) {
        return TextUtility.makeNormalAndSetDimension(this, str, R.dimen.text_size_body1);
    }

    private Spannable getBigSizeText(String str) {
        return TextUtility.SetDimension(this, str, R.dimen.text_size_subhead);
    }

    private Spannable getBoldAndBigSizeText(int res_str) {
        return TextUtility.makeBoldAndSetDimension(this,
                getString(res_str), R.dimen.text_size_subhead);
    }

    private Spannable getBoldAndBigSizeText(String str) {
        return TextUtility.makeBoldAndSetDimension(this,
                str, R.dimen.text_size_subhead);
    }

    @Override
    public void getCount(float value, CountView view) {
        Log.d(TAG, "getCount with " + value + " and view: " + view.hashCode());
        if (value > 0.0 && view.getId() == R.id.ndc_update_count_partialpack_layout) {
            enablePartialButton(true);
        }else if(value ==0.0 && view.getId() == R.id.ndc_update_count_partialpack_layout){
            enablePartialButton(false);
            Log.d("test","value of partial become zero");
        }
        viewValueMap.put(view, value);
        if (isNdcEntryManual) {
            calculateTotalAndPopulatePackValuesForManualEntry();
        } else {
            calculateTotalAndPopulatePackValues();
        }
    }


    public void enablePartialButton(boolean flagEnable) {
        mAddPartial.setEnabled(flagEnable);
    }


    private void calculateTotalAndPopulatePackValues() {
        float total = 0;

        partialPacks.clear();
        Log.d(TAG, "mcountarray size is " + viewValueMap.entrySet().size());
        for (Map.Entry<View, Float> viewFloatEntry : viewValueMap.entrySet()) {
            float value = viewFloatEntry.getValue();
            if (viewFloatEntry.getKey().getId() == R.id.ndc_update_count_fullpack_layout) {
                wholePacks = value * mPackSizeCount * mPacksPerContainer;
                total = total + (value * mPackSizeCount * mPacksPerContainer);// total+(value * mPackSizeCount)
            } else {
                partialPacks.add(value);
                total = total + (value); // total+(value * mPackSizeCount)
            }
        }
        totalValue = total;
        if (total >= 0) {
            setTotalCountTxt(total);
        }
    }

    private void calculateTotalAndPopulatePackValuesForManualEntry() {
        float total = 0;
        int count = 0;
        partialPacks.clear();
        Log.d(TAG, "mcountarray size is " + viewValueMap.size());
        for (Map.Entry<View, Float> viewFloatEntry : viewValueMap.entrySet()) {
            float value = viewFloatEntry.getValue();
            if (viewFloatEntry.getKey().getId() == R.id.ndc_update_count_fullpack_layout) {
                wholePacks = value;
                total = total + (value);// total+(value * mPackSizeCount)
            } else {
                count += 1;
                Log.d(TAG, "count =" + count);
                partialPacks.add(value);
                total = total + (value); // total+(value * mPackSizeCount)
            }
        }
        totalValue = total;
        if (total >= 0) {
            setTotalCountTxt(total);
        }
    }

    int i = 0;

    @OnClick(R.id.ndc_update_count_add_row)
    public void addRowClicked() {
        //addPartialPackCountView(0.0f);
        Toast.makeText(this,"add countview",Toast.LENGTH_LONG).show();
    }

    // remove 0.0 value/null value.input filter
    private void addPartialPackCountView(Float value) {
        Log.d(TAG, "mAddRowLayout child count: " + mAddRowLayout.getChildCount());
        if (mAddRowLayout.getChildCount() == 19) {
            Toast toast = Toast.makeText(this, "message", Toast.LENGTH_LONG);
            toast.setText("Cannot add any more partial counts");
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
            return;
        }
        CountView view = new CountView(this);
        if (isNdcEntryManual) {
            Log.d(TAG, "check for manual ndc entry inside add row clicked");
            view.setInputTypeToFloat();
        } else {
            if (uom.toLowerCase().contains("ml")) {
                view.setInputTypeToFloat();
            } else {
                view.setInputTypeToNumber();
            }
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(dpToPx(5), dpToPx(5), dpToPx(16), dpToPx(5));
        view.setLayoutParams(params);
        view.setDeleteVisible(true);
        view.addCountChangeListener(this, false);
        view.addCountDeletedListner(() -> {
            viewValueMap.remove(view);
            mAddRowLayout.removeView(view);
        });
        view.setId(i++);
        mAddRowLayout.addView(view);

        viewValueMap.put(view, value);
        view.setCount(value);
        mAddRowScrollView.post(() -> mAddRowScrollView.fullScroll(View.FOCUS_DOWN));
    }

    @OnClick(R.id.clear)
    public void clearClicked() {
        mPartialPackCount.clearText();
        mFullPackCount.clearText();
        mAddRowLayout.removeAllViews();
        viewValueMap.clear();
    }

    @OnClick(R.id.submit)
    public void submitClicked() {
        if (wholePacks == 0) {
            Log.d(TAG, "full pack count = 0");
            Toast toast = Toast.makeText(this, "message", Toast.LENGTH_LONG);
            toast.setText("Please enter valid counts to continue");
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        } else {

            UpdateCountController updateCountController = UpdateCountController.getInstance();
            UpdateCountController.UpdateCountCallback callback = new UpdateCountController.UpdateCountCallback() {

                @Override
                public void onUpdateCountPassed(TSSPJsonResponseForCountUpdate updateCountResponse) {
                    Log.d("debug", "Ndc count update successfull callback");
                    requestHandle = null;
                    runOnUiThread(() -> {
                        Log.d(TAG, "intent to go to scanndcactivity");
                        Intent intent = new Intent(getApplicationContext(), ScanNdcActivityTemp.class);
                        intent.putExtra("rackNum", rackNum);
                        intent.putExtra("NdcSubmitted", true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    });
                }

                @Override
                public void onUpdateCountFailed() {
                    requestHandle = null;
                    runOnUiThread(() -> showFailureDialog());
                }

                @Override
                public void onUpdateCountRequestTimeout() {
                    requestHandle = null;
                    runOnUiThread(() -> showFailureDialogForRequestTimeout());
                }

                @Override
                public void onNdcNotFound() {
                    requestHandle = null;
                    runOnUiThread(() -> showFailureDialog());
                }
            };

            Log.d("debug", "Calling method update count for ndc");
            requestHandle = updateCountController.updateCountForNdc(isNdcEntryManual,
                    LoginController.getInstance().getStoreNumber(),
                    LoginController.getInstance().getUsername(),
                    ndcSelected,
                    drug_name_manual,
                    wholePacks,
                    partialPacks,
                    totalValue,
                    rackNum,
                    callback);
        }
    }

    private void showFailureDialogForRequestTimeout() {
        AlertDialogManager.showFailureDialogForRequestTimeout(this, this.getClass());
    }

    private void showFailureDialog() {
        AlertDialogManager.showFailureDialog(this, this.getClass());
    }

    private int dpToPx(int dpValue) {
        float d = getResources().getDisplayMetrics().density;
        return (int) (dpValue * d); // margin in pixels
    }

    @Override
    void logoutUserCallback() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("IdleLogout", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
            Log.d(TAG, RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(0).getRackNo().toString());
            for (int i = 0; i < RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size(); i++) {
                if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                    Log.d(TAG, "If in progress");
                    if (LoginController.getInstance().getUsername().toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                        Log.d(TAG, "if username has scanned in progress");
                        updateRackStatusForRack(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackNo(), "PAUSE");
                    }
                }
            }
        }

        LoginController.getInstance().clearLoginResponse();
        AppDatabase.destroyInstance(this);
        startActivity(intent);


    }

    public void updateRackStatusForRack(String rackNum, String rackStatus) {
        RackStatusUpdateController rackStatusUpdateController = RackStatusUpdateController.getInstance();

        RackStatusUpdateController.RackStatusUpdateCallback callback = new RackStatusUpdateController.RackStatusUpdateCallback() {

            @Override
            public void onRackStatusUpdatePassed(RackStatusUpdateResponse rackStatusUpdateResponse) {

                // Handle scenario for status code ! = 000)
                Log.d("debug", "Rack status successfull callback");
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("debug", "Before call to show success dialog");
                        return;
                    }
                });
            }

            @Override
            public void onRackStatusUpdateFailed() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateDbAccessFailed() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestInvalid() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestTimeout() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //LoginController.getInstance().clearLoginResponse();
                        Log.d("TAG", "on login request timeout");
                        showFailureDialogForRequestTimeout();

                    }
                });
            }
        };

        Log.d("debug", "Calling method login using ID and password");
        EditText username = (EditText) findViewById(R.id.username_pi);
        Log.d(TAG, "find view by id username");

        EditText password = (EditText) findViewById(R.id.password_pi);
        Log.d(TAG, "find view by id password");

        requestHandle = rackStatusUpdateController.updateRackStatus(rackNum, rackStatus, LoginController.getInstance().getUsername(), LoginController.getInstance().getStoreNumber(),
                callback);
    }

}
