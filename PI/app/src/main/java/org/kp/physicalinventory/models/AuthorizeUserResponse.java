package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/1/17.
 */

public class AuthorizeUserResponse {

    private String[] region;
    private String[] userRole;

    private String timeOut;

    public String getRegion(int i) {
        return region[i];
    }

    public void setRegion(String[] region) {
        this.region = region;
    }

    public String[] getUserRole() {
        return userRole;
    }

    public String getUserRole(int i) {return userRole[i];}

    public String getTimeOut() { return timeOut; }

    public void setTimeOut(String timeOut) { this.timeOut = timeOut;}
}
