package org.kp.physicalinventory.models;

import java.util.List;

/**
 * Created by is-4586 on 12/19/17.
 */

public class ResponseForCheckIfCounted {
    private List<String> rackNo;
    private String alreadyCounted;
    private Float wholePack;
    private List<Float> partialPackQty;
    private Float totalQty;


    public String getAlreadyCounted() {
        return alreadyCounted;
    }

    public boolean isAlreadyCounted() {
        return alreadyCounted != null && alreadyCounted.contains("Y");
    }

    public void setAlreadyCounted(String alreadyCounted) {
        this.alreadyCounted = alreadyCounted;
    }

    public List<String> getRackNo() {
        return rackNo;
    }

    public void setRackNo(List<String> rackNo) {
        this.rackNo = rackNo;
    }

    public Float getWholePack() {
        return wholePack;
    }

    public void setWholePack(Float wholePack) {
        this.wholePack = wholePack;
    }

    public List<Float> getPartialPackQty() {
        return partialPackQty;
    }

    public void setPartialPackQty(List<Float> partialPackQty) {
        this.partialPackQty = partialPackQty;
    }

    public Float getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Float totalQty) {
        this.totalQty = totalQty;
    }
}
