package org.kp.physicalinventory.controllers;

import java.util.ArrayList;
import java.util.List;

import org.kp.physicalinventory.models.PharmacyInfo;
import org.kp.physicalinventory.models.PharmacyListResponse;

/**
 * Created by is-4586 on 11/1/17.
 */

public class AutoSuggestController {

    static public ArrayList<PharmacyInfo> startsWith(String text){

        List<PharmacyInfo> allPharmacies = PharmacyListController.getInstance().getPharmacyInfoList();
        ArrayList<PharmacyInfo> suggestList = new ArrayList<PharmacyInfo>();
        for (PharmacyInfo info: allPharmacies){
            if (info.getPhmAddress().toLowerCase().contains(text.toLowerCase())
                    || info.getPhmName().toLowerCase().contains(text.toLowerCase())
                        || info.getPhmNo().toLowerCase().contains(text.toLowerCase())
                            || info.getPhmCity().toLowerCase().contains(text.toLowerCase())
                                || info.getPhmZip().toLowerCase().contains(text.toLowerCase()))
            {
                suggestList.add(info);
            }
        }

        return suggestList;

    }
}
