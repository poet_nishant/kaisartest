package org.kp.physicalinventory.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackInfo {

    private String phmNo;
    private String rackNo;
    private String rackStatus;
    private String countedBy;

    public String getCountedBy() {
        return countedBy;
    }

    public void setCountedBy(String countedBy) {
        this.countedBy = countedBy;
    }

    public String getPhmNo() {
        return phmNo;
    }

    public void setPhmNo(String phmNo) {
        this.phmNo = phmNo;
    }

    public String getRackNo() {
        return rackNo;
    }

    public void setRackNo(String rackNo) {
        this.rackNo = rackNo;
    }

    public String getRackStatus() {
        return rackStatus;
    }

    public void setRackStatus(String rackStatus) {
        this.rackStatus = rackStatus;
    }


}
