package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class AssignedNdcRequest {

    private Boolean includeVariances;

    public Boolean getIncludeVariances() {
        return includeVariances;
    }

    public void setIncludeVariances(Boolean includeVariances) {
        this.includeVariances = includeVariances;
    }
}
