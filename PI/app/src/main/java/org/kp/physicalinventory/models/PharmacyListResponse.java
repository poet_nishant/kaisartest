package org.kp.physicalinventory.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by is-4586 on 11/2/17.
 */

public class PharmacyListResponse {

    @SerializedName("TSSPJsonResponse")
    private TSSPJsonResponseForPharmacyList tsspJsonResponse;
    private static final PharmacyListResponse instance = new PharmacyListResponse();
    private PharmacyListResponse() { }
    private static PharmacyListResponse Instance;

    public TSSPJsonResponseForPharmacyList getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForPharmacyList tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }

    public static PharmacyListResponse getInstance() {
        return Instance;
    }

    public static void setInstance(PharmacyListResponse instance) {
        Instance = instance;
    }
}
