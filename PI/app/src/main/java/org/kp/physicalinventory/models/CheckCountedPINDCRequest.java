package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 12/19/17.
 */

public class CheckCountedPINDCRequest {
    private String ndc;

    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }
}
