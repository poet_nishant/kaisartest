package org.kp.physicalinventory.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by is-4586 on 11/2/17.
 */

@Entity(tableName = "ndc")
public class NDC {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "ndc")
    private String ndc;

    @ColumnInfo(name = "drug_name")
    private String drugName;

    @ColumnInfo(name = "schedule")
    private String schedule;

    @ColumnInfo(name = "pack")
    private float pack;

    @ColumnInfo(name = "packs_per_container")
    private int packsPerContainer;

    @ColumnInfo(name = "manufacturer")
    private String manufacturer;

    @ColumnInfo(name = "uom")
    private String uom;

    @ColumnInfo(name = "gpi")
    private String gpi;

    @ColumnInfo(name = "barcode")
    private String barcode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public float getPack() {
        return pack;
    }

    public void setPack(float pack) {
        this.pack = pack;
    }

    public int getPacksPerContainer() {
        return packsPerContainer;
    }

    public void setPacksPerContainer(int packsPerContainer) {
        this.packsPerContainer = packsPerContainer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getGpi() {
        return gpi;
    }

    public void setGpi(String gpi) {
        this.gpi = gpi;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
