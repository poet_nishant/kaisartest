//package org.kp.physicalinventory.controllers;
//
///**
// * Created by is-4586 on 11/2/17.
// */
//
//import org.kp.physicalinventory.models.NDC;
//import org.kp.physicalinventory.models.NdcListResponse;
//
//public class ScannedNdcController {
//
//    private static NDC scannedNdc;
//
//    public static NDC getScannedNdc() {
//        return scannedNdc;
//    }
//
//    public static void setScannedNdc(NDC scannedNdc) {
//        ScannedNdcController.scannedNdc = scannedNdc;
//    }
//
//    public static void GetNDCFromBarcode(String barcode){
//        for(int i=0; i< NdcListResponse.getInstance().getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC().size(); i++){
//            if(NdcListResponse.getInstance().getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC(i).getBarcode() == barcode){
//                setScannedNdc(NdcListResponse.getInstance().getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC(i));
//            }
//        }
//
//    }
//
//    public static String GetUOM(){
//        return scannedNdc.getUom();
//    }
//
//    public static String GetGPI(){
//        return scannedNdc.getGpi();
//    }
//
//    public static String GetDrugName(){
//        return scannedNdc.getDrugName();
//    }
//
//    public static String GetSchedule(){
//        return scannedNdc.getSchedule();
//    }
//
//    public static String GetDrugcode(){
//        return scannedNdc.getPiNDC();
//    }
//
//    public static String GetManufacturer(){
//        return scannedNdc.getManufacturer();
//    }
//
//    public static int GetPackSize(){
//        return scannedNdc.getPack();
//    }
//
//    public static int GetPacksPerContainer(){
//        return scannedNdc.getPacksPerContainer();
//    }
//
//
//
//    //get full size = fullsize * packsize * packspercontainer
//
//}
