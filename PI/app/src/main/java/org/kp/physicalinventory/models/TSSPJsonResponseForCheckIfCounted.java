package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class TSSPJsonResponseForCheckIfCounted {

    private String statuscode;
    private String statusdesc;
    private String transactionid;
    private CheckIfCountedResponseWrapper response;
    
    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusdesc() {
        return statusdesc;
    }

    public void setStatusdesc(String statusdesc) {
        this.statusdesc = statusdesc;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }


    public CheckIfCountedResponseWrapper getResponse() {
        return response;
    }

    public void setResponse(CheckIfCountedResponseWrapper response) {
        this.response = response;
    }
}
