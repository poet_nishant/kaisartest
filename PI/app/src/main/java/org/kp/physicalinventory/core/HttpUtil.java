package org.kp.physicalinventory.core;

/**
 * Created by is-4586 on 11/6/17.
 */

import com.google.gson.Gson;
import com.loopj.android.http.*;
import com.loopj.android.http.RequestParams;
import java.io.UnsupportedEncodingException;
import java.lang.Object;
import java.nio.charset.UnsupportedCharsetException;
import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.UnsupportedHttpVersionException;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class HttpUtil {

    public static RequestHandle httpCall(Object requestData, JsonHttpResponseHandler responseHandler){

        String url = "https://pzabwa14.pldc.kp.org:15046/CycleCountRestServices/ccJSON/CCRestServices";
        Log.d("debug", "The URL is: " + url);
        AsyncHttpClient client = new AsyncHttpClient();
        //RequestParams params = new RequestParams();

        client.setTimeout(70000);
       // client.setConnectTimeout(100);
        //client.setResponseTimeout(100);
        //String httpResponseBody = null;
        Gson gson = new Gson();
        String json = gson.toJson(requestData);
        Log.d("debug", "The json string is: " + json);

        StringEntity entity = new StringEntity(json, "UTF-8");

//        ByteArrayEntity entity = new ByteArrayEntity(requestData.toString().getBytes("UTF-8"));
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        Log.d("debug", "set content in entity");

        Log.d("debug", "before HTTP put");
        return client.put(null, url, entity, "application/json", responseHandler);
    }
}
