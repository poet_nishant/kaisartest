package org.kp.physicalinventory.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.util.Log;

import org.kp.physicalinventory.dao.NdcDao;
import org.kp.physicalinventory.models.NDC;

@Database(entities = {NDC.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String TAG = AppDatabase.class.getSimpleName();
    private static AppDatabase INSTANCE;

    public abstract NdcDao ndcDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "ndc-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance(Context context) {
        Log.d(TAG, "inside destroy instance before checking instance = null");
        if (INSTANCE != null) {
            Log.d(TAG, "inside destroy instance, before destroy, the size is" + INSTANCE.ndcDao().getAll().size());
            INSTANCE.ndcDao().deleteAll();
            Log.d(TAG, "inside destroy instance, after destroy, the size is" + INSTANCE.ndcDao().getAll().size());
        } else {
            Log.d(TAG, "gettingAppDatabase and Clearing Database");
            getAppDatabase(context);
            INSTANCE.ndcDao().deleteAll();
        }
        INSTANCE = null;
    }

    public static boolean isEmpty(){
        if (INSTANCE == null){
            return true;
        }else
        {
            return false;
        }
    }
}
