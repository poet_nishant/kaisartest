package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/30/17.
 */

public class CheckIfCountedResponse {

    private TSSPJsonResponseForCheckIfCounted TSSPJsonResponse;

    public TSSPJsonResponseForCheckIfCounted getTSSPJsonResponse() {
        return TSSPJsonResponse;
    }

    public void setTSSPJsonResponse(TSSPJsonResponseForCheckIfCounted TSSPJsonResponse) {
        this.TSSPJsonResponse = TSSPJsonResponse;
    }
}
