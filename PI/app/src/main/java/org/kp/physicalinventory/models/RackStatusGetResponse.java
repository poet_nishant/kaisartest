package org.kp.physicalinventory.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusGetResponse {

    @SerializedName("TSSPJsonResponse")
    private TSSPJsonResponseForGetRack tsspJsonResponse;

    public TSSPJsonResponseForGetRack getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForGetRack tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }
}
