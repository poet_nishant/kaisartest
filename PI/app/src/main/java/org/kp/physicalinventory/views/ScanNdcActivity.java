package org.kp.physicalinventory.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.RequestHandle;
//import com.symbol.emdk.EMDKManager;
//import com.symbol.emdk.EMDKManager.EMDKListener;
//import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
//import com.symbol.emdk.EMDKResults;
//import com.symbol.emdk.barcode.BarcodeManager;
//import com.symbol.emdk.barcode.ScanDataCollection;
//import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
//import com.symbol.emdk.barcode.Scanner;
//import com.symbol.emdk.barcode.Scanner.DataListener;
//import com.symbol.emdk.barcode.Scanner.StatusListener;
//import com.symbol.emdk.barcode.Scanner.TriggerType;
//import com.symbol.emdk.barcode.ScannerConfig;
//import com.symbol.emdk.barcode.ScannerException;
//import com.symbol.emdk.barcode.ScannerInfo;
//import com.symbol.emdk.barcode.ScannerResults;
//import com.symbol.emdk.barcode.StatusData;
//import com.symbol.emdk.barcode.StatusData.ScannerStates;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.AlertDialogManager;
import org.kp.physicalinventory.controllers.CheckIfCountedController;
import org.kp.physicalinventory.controllers.LoginController;
import org.kp.physicalinventory.controllers.NdcListController;
import org.kp.physicalinventory.controllers.RackStatusGetController;
import org.kp.physicalinventory.controllers.RackStatusUpdateController;
import org.kp.physicalinventory.core.DatabaseInitializer;
import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.CheckIfCountedResponse;
import org.kp.physicalinventory.models.NDC;
import org.kp.physicalinventory.models.NDCListResponse;
import org.kp.physicalinventory.models.RackStatusUpdateResponse;
import org.kp.physicalinventory.models.ResponseForCheckIfCounted;

import java.util.ArrayList;
//DataListener, StatusListener,
public class ScanNdcActivity extends BaseActivity implements  View.OnClickListener {

    private static final String TAG = ScanNdcActivity.class.getSimpleName();
    public static final String SELECTED_NDC_EXTRA = "selectedNdc";
    public static final String RACK_NUM_EXTRA = "rackNum";
    public static final String CHECK_IF_COUNTED_RESPONSE = "checkIfCountedResponse" ;
    RequestHandle requestHandle = null;
    private String storeId;
    private String username;
//    private EMDKManager emdkManager = null;
//    private BarcodeManager barcodeManager = null;
//    private Scanner scanner = null;
    private String statusString = "";
    private boolean bContinuousMode;
    private Button btnScanBarCode;
    private Button btnSubmitNumber;
    private Button btnSubmitName;
    private EditText edtNDCName;
    private EditText edtNDCNumber;
    private RelativeLayout relNDCNameContainer;
    private View dividerView;
    private RelativeLayout errorContainer;
    private NDC typedNdc;
    private String selectedRackNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ndc_scan);
        Log.d(TAG, "On create of Scan Ndc Activity");

        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        if (bundle != null && bundle.getBoolean("NdcSubmitted")) {
            Log.d(TAG, "on create of scan ndc, if bundle is not equal to null");
            Toast toast = Toast.makeText(this, "message", Toast.LENGTH_LONG);
            toast.setText("NDC submitted");
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }
        selectedRackNumber = i.getStringExtra("rackNum");
        //String rack_num_selected = "11111";
        storeId = LoginController.getInstance().getStoreNumber();
        //storeId = "900003006";
        username = LoginController.getInstance().getUsername();
        //username = "W081411";

        Toolbar toolbar_scan = (Toolbar) findViewById(R.id.toolbar);
        toolbar_scan.setTitle("Scan NDC for Section " + selectedRackNumber);
        setSupportActionBar(toolbar_scan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getNDCList();
        initViewAndListener();
        //initEmdkAndScanner();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent i = new Intent(this, RackViewActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    private void initEmdkAndScanner() {
//        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), new EMDKListener() {
//            @Override
//            public void onOpened(EMDKManager emdkManager) {
//                Log.d(TAG, "Inside onOpened, status :emdk open success");
//                ScanNdcActivity.this.emdkManager = emdkManager;
//
//                // Acquire the barcode manager resources
//                barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
//
//                // Add connection listener
//                if (barcodeManager != null) {
//                    Log.d(TAG, "After acquiring barcode manager resources, checking if barcodeManager!=null");
//                }
//            }
//
//            @Override
//            public void onClosed() {
//                if (emdkManager != null) {
//
//                    // Remove connection listener
//                    if (barcodeManager != null) {
//                        // barcodeManager.removeConnectionListener(this);
//                        barcodeManager = null;
//                    }
//
//                    // Release all the resources
//                    emdkManager.release();
//                    emdkManager = null;
//                }
//            }
//        });
//
//        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
//            Log.d(TAG, "If results.statusCode != sucess, show failure message");
//        }
//    }

    private void initViewAndListener() {
        btnScanBarCode = (Button) findViewById(R.id.btnScanBarCode);
        btnSubmitNumber = (Button) findViewById(R.id.btnSubmitNumber);
        btnSubmitName = (Button) findViewById(R.id.btnSubmitName);
        btnSubmitName.setEnabled(false);
        btnSubmitName.getBackground().setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.MULTIPLY);
        edtNDCNumber = (EditText) findViewById(R.id.edtNDCNumber);
        edtNDCName = (EditText) findViewById(R.id.edtNDCName);
        errorContainer = (RelativeLayout) findViewById(R.id.errorContainer);
        dividerView = (View) findViewById(R.id.divider);
        relNDCNameContainer = (RelativeLayout) findViewById(R.id.relNDCNameContainer);
        btnSubmitName.setOnClickListener(this);
        btnScanBarCode.setOnClickListener(this);
        btnSubmitNumber.setOnClickListener(this);

        edtNDCNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                enableSubmitIfReady();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        edtNDCNumber.setOnTouchListener((view, motionEvent) -> {
            btnScanBarCode.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
            return false;
        });

        edtNDCName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                enableSubmitIfReadyNDCName();
            }
        });
    }

    private void enableSubmitIfReady() {
        LinearLayout til = (LinearLayout) findViewById(R.id.text_input_layout);

        boolean isReady = edtNDCNumber.getText().toString().length() >= 11;
        if (isReady) {
            // check the ndc in arraylist , if found press submit button and go to next screen.
            // if not found display the error message shown in red  and allow user to enter one more field drug name.
            // store it in Intent and pass it to next screen.
            boolean isfoundNdc = scanNDCList(edtNDCNumber.getText().toString());
            if (!isfoundNdc) {
                errorContainer.setVisibility(View.VISIBLE);
                relNDCNameContainer.setVisibility(View.VISIBLE);
                btnSubmitNumber.setVisibility(View.INVISIBLE);
            }
        } else {
            relNDCNameContainer.setVisibility(View.INVISIBLE);
            btnSubmitNumber.setVisibility(View.VISIBLE);
            errorContainer.setVisibility(View.INVISIBLE);
        }
        btnSubmitNumber.setEnabled(isReady);
    }

    private void enableSubmitIfReadyNDCName() {
        RelativeLayout ndcNameLayout = (RelativeLayout) findViewById(R.id.relNDCNameContainer);
        boolean isReady = edtNDCName.getText().toString().length() >= 1;
        if (isReady) {
            btnSubmitName.setEnabled(true);
        }
        btnSubmitName.setEnabled(isReady);
    }

    private boolean scanNDCList(String ndc) {

        boolean isFound = false;
        AppDatabase db = AppDatabase.getAppDatabase(this);
        if (db.ndcDao().findByNdc(ndc) != null) {
            isFound = true;
            typedNdc = db.ndcDao().findByNdc(ndc);
        }
        return isFound;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmitNumber:
                CheckIfCountedController.getInstance().checkIfAlreadyCounted(
                        storeId, username, typedNdc.getNdc(),
                        new CheckIfCountedController.CheckIfCountedCallback() {
                            @Override
                            public void onPassed(CheckIfCountedResponse checkIfCountedResponse) {
                                ResponseForCheckIfCounted response = checkIfCountedResponse.getTSSPJsonResponse().getResponse().getCheckCountedPINDCResponse();
                                if (response.isAlreadyCounted()) {
                                    showDialogForAlreadyCounted(typedNdc, response);
                                } else {
                                    Intent i = new Intent(ScanNdcActivity.this, NDCUpdateCountActivity.class);
                                    Gson gson = new Gson();
                                    i.putExtra(SELECTED_NDC_EXTRA, gson.toJson(typedNdc));
                                    i.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
                                    startActivity(i);
                                }
                            }

                            @Override
                            public void onDbAccessFailed() {
                                AlertDialogManager.showUnableToConnectDialog(ScanNdcActivity.this);
                            }

                            @Override
                            public void onRequestTimeout() {
                                AlertDialogManager.showUnableToConnectDialog2(ScanNdcActivity.this);
                            }
                        });

                break;

            case R.id.btnScanBarCode:
          //      startScan();
                break;

            case R.id.btnSubmitName:
                Intent intent = new Intent(this, NDCUpdateCountActivity.class);
                if (edtNDCName.getText() != null && !"".equals(edtNDCName.getText().toString())) {
                    CheckIfCountedController.getInstance().checkIfAlreadyCounted(
                            storeId, username, edtNDCNumber.getText().toString(),
                            new CheckIfCountedController.CheckIfCountedCallback() {
                                @Override
                                public void onPassed(CheckIfCountedResponse checkIfCountedResponse) {
                                    ResponseForCheckIfCounted response = checkIfCountedResponse.getTSSPJsonResponse().getResponse().getCheckCountedPINDCResponse();
                                    if (response.isAlreadyCounted()) {
                                        showDialogForAlreadyCounted(edtNDCNumber.getText().toString(), response);
                                    } else {
                                        showDialogForManualDrugEntry();
//                                        intent.putExtra("selectedNdc", edtNDCNumber.getText().toString());
//                                        intent.putExtra("selectedDrugName", edtNDCName.getText().toString());
//                                        intent.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
//                                        intent.putExtra("DrugnameManualEntry", true);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
                                    }
                                }

                                @Override
                                public void onDbAccessFailed() {
                                    AlertDialogManager.showUnableToConnectDialog(ScanNdcActivity.this);
                                }

                                @Override
                                public void onRequestTimeout() {
                                    AlertDialogManager.showUnableToConnectDialog2(ScanNdcActivity.this);
                                }
                            });

                }
                break;
        }
    }

    private void showDialogForManualDrugEntry(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle("Manual drug entry");

        // set dialog message
        alertDialogBuilder.setMessage("Pack size information for manual entry is not present. Please enter the drug counts in LUOM only");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Dismiss", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setNegativeButton("Continue", (dialogInterface, i) -> {
            Intent intent = new Intent(this, NDCUpdateCountActivity.class);
            intent.putExtra("selectedNdc", edtNDCNumber.getText().toString());
            intent.putExtra("selectedDrugName", edtNDCName.getText().toString());
            intent.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
            intent.putExtra("DrugnameManualEntry", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        });
        alertDialogBuilder.show();
    }

    private void showDialog() {
        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(ScanNdcActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Scan NDC");
        dialog.setMessage("Are you sure you want to Scan NDC?");
        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        })
                .setNegativeButton("YES ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final android.app.AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // De-initialize scanner
        //deInitScanner();

        Log.d(TAG, "Inside onDestroy of activity");
        // Remove connection listener
//        if (barcodeManager != null) {
//            // barcodeManager.removeConnectionListener(this);
//            barcodeManager = null;
//        }
//
//        // Release all the resources
//        if (emdkManager != null) {
//            emdkManager.release();
//            emdkManager = null;
//        }
    }

    @Override
    void logoutUserCallback() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("IdleLogout", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse() != null) {
            Log.d(TAG, RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(0).getRackNo().toString());
            for (int i = 0; i < RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo().size(); i++) {
                if ("IN_PROGRESS".equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackStatus())) {
                    Log.d(TAG, "If in progress");
                    if (LoginController.getInstance().getUsername().toLowerCase().equals(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getCountedBy().toLowerCase())) {
                        Log.d(TAG, "if username has scanned in progress");
                        updateRackStatusForRack(RackStatusGetController.getInstance().getRackStatusGetResponse().getTsspJsonResponse().getResponse().getGetRackStatusResponse().getRackInfo(i).getRackNo(), "PAUSE");
                    }
                }
            }
        }

        LoginController.getInstance().clearLoginResponse();
        AppDatabase.destroyInstance(this);
        startActivity(intent);


    }

    public void updateRackStatusForRack(String rackNum, String rackStatus) {
        RackStatusUpdateController rackStatusUpdateController = RackStatusUpdateController.getInstance();

        RackStatusUpdateController.RackStatusUpdateCallback callback = new RackStatusUpdateController.RackStatusUpdateCallback() {

            @Override
            public void onRackStatusUpdatePassed(RackStatusUpdateResponse rackStatusUpdateResponse) {

                // Handle scenario for status code ! = 000)
                Log.d("debug", "Rack status successfull callback");
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("debug", "Before call to show success dialog");
                        return;
                    }
                });
            }

            @Override
            public void onRackStatusUpdateFailed() {
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateDbAccessFailed(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestInvalid(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailureDialog();
                    }
                });
            }

            public void onRackStatusUpdateRequestTimeout(){
                requestHandle = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //LoginController.getInstance().clearLoginResponse();
                        Log.d("TAG", "on login request timeout");
                        showFailureDialogForRequestTimeout();

                    }
                });
            }
        };

        Log.d("debug", "Calling method login using ID and password");
        EditText username = (EditText)findViewById(R.id.username_pi);
        Log.d(TAG, "find view by id username");

        EditText password = (EditText)findViewById(R.id.password_pi) ;
        Log.d(TAG, "find view by id password");

        requestHandle = rackStatusUpdateController.updateRackStatus(rackNum, rackStatus, LoginController.getInstance().getUsername(), LoginController.getInstance().getStoreNumber(),
                callback);
    }
    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background

        // De-initialize scanner
       // deInitScanner();

        Log.d(TAG, "Inside onPause of activity");

        // Remove connection listener
//        if (barcodeManager != null) {
//            // barcodeManager.removeConnectionListener(this);
//            barcodeManager = null;
//            //deviceList = null;
//        }
//
//        // Release the barcode manager resources
//        if (emdkManager != null) {
//            emdkManager.release(FEATURE_TYPE.BARCODE);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Acquire the barcode manager resources
//        if (emdkManager != null) {
//            barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
//            // Initialize scanner
//            initScanner();
//            setDecoders();
//        }
    }

//    @Override
//    public void onData(ScanDataCollection scanDataCollection) {
//
//        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
//            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
//            for (ScanData data : scanData) {
//
//                String dataString = data.getData();
//
//                new ScanNdcActivity.AsyncDataUpdate().execute(dataString);
//            }
//
////            String dataString = scanDataCollection.getScanData().toString();
////            Log.d(TAG, dataString);
////            new AsyncDataUpdate().execute(dataString);
//
//        }
//    }
//
//    @Override
//    public void onStatus(StatusData statusData) {
//
//        ScannerStates state = statusData.getState();
//        switch (state) {
//            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                //new AsyncStatusUpdate().execute(statusString);
//                if (bContinuousMode) {
//                    try {
//                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
//                        // may cause the scanner to pause momentarily before resuming the scanning.
//                        // Hence add some delay (>= 100ms) before submitting the next read.
//                        try {
//                            Thread.sleep(100);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//
//                        scanner.read();
//                    } catch (ScannerException e) {
//                        //statusString = e.getMessage();
//                        //new AsyncStatusUpdate().execute(statusString);
//                    }
//                }
//                //new AsyncUiControlUpdate().execute(true);
//                break;
//            case WAITING:
//                statusString = "Scanner is waiting for trigger press...";
//                new ScanNdcActivity.AsyncStatusUpdate().execute(statusString);
//                //new AsyncUiControlUpdate().execute(false);
//                break;
//            case SCANNING:
//                statusString = "Scanning...";
//                new ScanNdcActivity.AsyncStatusUpdate().execute(statusString);
//                // new AsyncUiControlUpdate().execute(false);
//                break;
//            case DISABLED:
//                statusString = statusData.getFriendlyName() + " is disabled.";
//                new ScanNdcActivity.AsyncStatusUpdate().execute(statusString);
//                //new AsyncUiControlUpdate().execute(true);
//                break;
//            case ERROR:
//                statusString = "An error has occurred.";
//                new ScanNdcActivity.AsyncStatusUpdate().execute(statusString);
//                //new AsyncUiControlUpdate().execute(true);
//                break;
//            default:
//                break;
//        }
//    }

//    //
//    private void addStartScanButtonListener() {
//
//        Button btnStartScan = (Button)findViewById(R.id.buttonStartScan);
//
//        btnStartScan.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                startScan();
//            }
//        });
//    }

//    private void setTrigger(@NonNull Scanner scanner) {
//        scanner.triggerType = TriggerType.SOFT_ALWAYS;
//    }

//    private void setDecoders() {
//
//        if (scanner == null) {
//            initScanner();
//        }
//
//        if (scanner != null) {
//            try {
//
//                ScannerConfig config = scanner.getConfig();
//                config.decoderParams.code39.enabled = true;
//                scanner.setConfig(config);
//
////                // Set EAN8
////                if(checkBoxEAN8.isChecked())
////                    config.decoderParams.ean8.enabled = true;
////                else
////                    config.decoderParams.ean8.enabled = false;
////
////                // Set EAN13
////                if(checkBoxEAN13.isChecked())
////                    config.decoderParams.ean13.enabled = true;
////                else
////                    config.decoderParams.ean13.enabled = false;
////
////                // Set Code39
////                if(checkBoxCode39.isChecked())
////
////                else
////                    config.decoderParams.code39.enabled = false;
////
////                //Set Code128
////                if(checkBoxCode128.isChecked())
////                    config.decoderParams.code128.enabled = true;
////                else
////                    config.decoderParams.code128.enabled = false;
//
////            } catch (ScannerException e) {
//
////                textViewStatus.setText("Status: " + e.getMessage());
//            }
//        }
//    }


//    private void startScan() {
//
//        if (scanner == null) {
//            initScanner();
//        }
//
//        if (scanner != null) {
//            try {
//
//                // Submit a new read.
//                scanner.read();
//
////                if (checkBoxContinuous.isChecked())
////                    bContinuousMode = true;
////                else
////                    bContinuousMode = false;
//
//                bContinuousMode = false;
//
//                //new AsyncUiControlUpdate().execute(false);
//
//            } catch (ScannerException e) {
////                textViewStatus.setText("Status: " + e.getMessage());
//            }
//        }
//
//    }

//    private void initScanner() {
//
//        if (scanner == null) {
//
//            Log.d(TAG, "init scanner, if scanner == null");
//            ScannerInfo scanInfo = barcodeManager.getSupportedDevicesInfo().get(0);
//            Log.d(TAG, "The supported device selected is: " + scanInfo.getFriendlyName());
//            scanner = barcodeManager.getDevice(scanInfo);
//
//            if (scanner != null) {
//                setTrigger(scanner);
//                scanner.addDataListener(this);
//                scanner.addStatusListener(this);
//
//                try {
//                    scanner.enable();
//                } catch (ScannerException e) {
//                    Log.d(TAG, "Status: " + e.getMessage());
//                }
//            }
//        }
//    }

//    private void deInitScanner() {
//
//        if (scanner != null) {
//            Log.d(TAG, "Inside deInit scanner, if scanner != null");
//            try {
//
//                scanner.cancelRead();
//                scanner.disable();
//
//            } catch (ScannerException e) {
//                Log.d(TAG, "Status: " + e.getMessage());
//            }
//            scanner.removeDataListener(this);
//            scanner.removeStatusListener(this);
//            try {
//                scanner.release();
//            } catch (ScannerException e) {
//                Log.d(TAG, "Status: " + e.getMessage());
//            }
//
//            scanner = null;
//        }
//    }

    private class AsyncDataUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "Async data update, do in background");
            Log.d(TAG, "Async data update do in background" + params[0]);
            return params[0];


        }

        protected void onPostExecute(String result) {

            if (result != null) {

                Log.d(TAG, result);
                getBarcodeToNdcMapping(result);
            }
        }
    }

    private void getBarcodeToNdcMapping(String result) {
        AppDatabase db = AppDatabase.getAppDatabase(this);
        Log.d(TAG, result);
        final NDC foundNDC = db.ndcDao().findByBarcode(result);

        if (foundNDC != null) {
            Log.d(TAG, "Found NDC from barcode:" + foundNDC.getDrugName());
            //check if already counted, if yes alert dialog
            //check restricted access- controlled substance
            if ("TECHNICIAN".equals(LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getUserRole(0))) {
                Log.d(TAG, "user role is " + LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getUserRole(0));
                if ("2".equals(foundNDC.getSchedule()) || "11".equals(foundNDC.getSchedule())) {
                    showAlertDialogForRestrictedAccess();
                    return;
                }
            }

            CheckIfCountedController.getInstance().checkIfAlreadyCounted(
                    storeId, username, foundNDC.getNdc(),
                    new CheckIfCountedController.CheckIfCountedCallback() {
                        @Override
                        public void onPassed(CheckIfCountedResponse checkIfCountedResponse) {
                            ResponseForCheckIfCounted response = checkIfCountedResponse.getTSSPJsonResponse().getResponse().getCheckCountedPINDCResponse();
                            if (response.isAlreadyCounted()) {
                                showDialogForAlreadyCounted(foundNDC, response);
                            } else {
                                Intent i = new Intent(ScanNdcActivity.this, NDCUpdateCountActivity.class);
                                Gson gson = new Gson();
                                i.putExtra(SELECTED_NDC_EXTRA, gson.toJson(foundNDC));
                                i.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onDbAccessFailed() {
                            AlertDialogManager.showUnableToConnectDialog(ScanNdcActivity.this);
                        }

                        @Override
                        public void onRequestTimeout() {
                            AlertDialogManager.showUnableToConnectDialog2(ScanNdcActivity.this);
                        }
                    });




        } else {
            AlertDialogManager.showDialogForBarcodeNotFoundInList(this);
        }

    }



//    private void showDialogForBarcodeNotFoundInList(){
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        // set title
//        alertDialogBuilder.setTitle("NDC not found");
//
//        // set dialog message
//        alertDialogBuilder
//                .setMessage("The NDC scanned was not found in the system. Please manually enter the NDC number and name to proceed with counting")
//                .setCancelable(false)
//                .setPositiveButton("DISMISS", null);
//
//        // create alert dialog
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        // show it
//        alertDialog.show();
//    }

    private class AsyncStatusUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "Do in background of Async status update");
            return params[0];
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "On post execute of async status update");

        }
    }


    private void getNDCList() {

        Log.d(TAG, "Inside get ndc list");
        AppDatabase db = AppDatabase.getAppDatabase(this);
        Log.d(TAG, "The size is:" + db.ndcDao().getAll().size());

        if (db.ndcDao().getAll().size() == 0) {
            NdcListController ndcListController = NdcListController.getInstance();
            NdcListController.NdcListFetchCallback callback = new NdcListController.NdcListFetchCallback() {
                @Override
                public void onNdcListReceived(NDCListResponse ndcListResponse) {

                    // Handle scenario for status code ! = 000)
                    Log.d(TAG, "Ndc list received successfull callback");
                    requestHandle = null;
                    runOnUiThread(() -> {
                        Log.d(TAG, "Before call to show success dialog");

                        ArrayList<NDC> ndcList = new ArrayList<NDC>();
                        if (ndcListResponse.getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC() != null) {
                            int size = ndcListResponse.getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC().size();
                            for (int i = 0; i < size; i++) {
                                ndcList.add(ndcListResponse.getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getNdc(i));
                            }
                            DatabaseInitializer.populateAsync(AppDatabase.getAppDatabase(getApplicationContext()), ndcList);
                        }
                    });
                }

                @Override
                public void onNdcListRequestTimeout() {
                    Log.d(TAG, "Ndc list TIMEOUT");
                    requestHandle = null;
                    runOnUiThread(() -> showFailureDialogForRequestTimeout());
                }

                @Override
                public void onNdcListFetchFailed() {
                    Log.d(TAG, "Ndc list Fetch Failed");
                    requestHandle = null;
                    runOnUiThread(() -> {
                        //LoginController.getInstance().clearLoginResponse();
                        showFailureDialog();
                    });
                }

            };
            requestHandle = ndcListController.getNdcList(storeId, username, true, callback);
        }
    }

    private void showFailureDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage(R.string.unable_to_reach_server);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setPositiveButton(R.string.try_again, (dialogInterface, i) -> {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(600, 400);
    }

    private void showFailureDialogForRequestTimeout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage(R.string.unable_to_connect_dialog_body);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setNegativeButton(R.string.try_again, (dialogInterface, i) -> {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        });
    }

    private void showAlertDialogForRestrictedAccess(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle(R.string.ndc_access_restricted_dialog_title);

        // set dialog message
        alertDialogBuilder.setMessage(R.string.ndc_access_restricted_dialog_body);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("Dismiss", (dialogInterface, i) -> dialogInterface.dismiss());
       alertDialogBuilder.show();
    }

    private void showDialogForAlreadyCounted(NDC foundNDC, ResponseForCheckIfCounted response){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle(R.string.ndc_already_counted_dialog_title);

        // set dialog message
        alertDialogBuilder.setMessage(R.string.ndc_already_counted_dialog_body);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Dismiss", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setNegativeButton("Continue", (dialogInterface, i) -> {
            Intent intent = new Intent(ScanNdcActivity.this, NDCUpdateCountActivity.class);
            Gson gson = new Gson();
            intent.putExtra(SELECTED_NDC_EXTRA, gson.toJson(foundNDC));
            intent.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
            intent.putExtra(CHECK_IF_COUNTED_RESPONSE, gson.toJson(response));
            intent.putExtra("AlreadyCounted", true);
            startActivity(intent);

        });
        alertDialogBuilder.show();
    }

    private void showDialogForAlreadyCounted(String ndc, ResponseForCheckIfCounted response){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle(R.string.ndc_already_counted_dialog_title);

        // set dialog message
        alertDialogBuilder.setMessage(R.string.ndc_already_counted_dialog_body);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Dismiss", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.setNegativeButton("Continue", (dialogInterface, i) -> {
            Intent intent = new Intent(ScanNdcActivity.this, NDCUpdateCountActivity.class);
            Gson gson = new Gson();
            intent.putExtra("selectedNdc", edtNDCNumber.getText().toString());
            intent.putExtra("selectedDrugName", edtNDCName.getText().toString());
            intent.putExtra(RACK_NUM_EXTRA, selectedRackNumber);
            intent.putExtra("DrugnameManualEntry", true);
            intent.putExtra(CHECK_IF_COUNTED_RESPONSE, gson.toJson(response));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("AlreadyCounted", true);
            startActivity(intent);

        });
        alertDialogBuilder.show();
    }



}
