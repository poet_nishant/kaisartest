package org.kp.physicalinventory.models;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/21/17.
 */

public class GetRackStatusResponse {

    public ArrayList<RackInfo> getRackInfo() {
        return rackInfo;
    }

    public RackInfo getRackInfo(int i) { return rackInfo.get(i); }

    public void setRackInfo(ArrayList<RackInfo> rackInfo) {
        this.rackInfo = rackInfo;
    }

    private ArrayList<RackInfo> rackInfo;
}
