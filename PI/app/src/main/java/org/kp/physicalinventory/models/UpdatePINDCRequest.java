package org.kp.physicalinventory.models;

import java.util.List;

/**
 * Created by is-4586 on 11/2/17.
 */

public class UpdatePINDCRequest {

    private String ndc;
    private float wholePack;
    private List<Float> partialPack;
    private float totalqty;
    private String rackNo;
    private String drugName;

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }



    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }

    public float getWholePack() {
        return wholePack;
    }

    public void setWholePack(float wholePack) {
        this.wholePack = wholePack;
    }

    public List<Float> getPartialPack() {
        return partialPack;
    }

    public void setPartialPack(List<Float> partialPack) {
        this.partialPack = partialPack;
    }

    public String getRackNo() {
        return rackNo;
    }

    public void setRackNo(String rackNo) {
        this.rackNo = rackNo;
    }

    public float getTotalQty() {
        return totalqty;
    }

    public void setTotalQty(float totalqty) {
        this.totalqty = totalqty;
    }
}
