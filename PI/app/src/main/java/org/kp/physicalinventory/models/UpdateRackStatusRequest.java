package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/21/17.
 */

public class UpdateRackStatusRequest {

    private String rackNo;
    private String rackStatus;

    public String getRackNo() {
        return rackNo;
    }

    public void setRackNo(String rackNo) {
        this.rackNo = rackNo;
    }

    public String getRackStatus() {
        return rackStatus;
    }

    public void setRackStatus(String rackStatus) {
        this.rackStatus = rackStatus;
    }
}
