package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class TSSPJsonResponseForCountUpdate {

    private String statuscode;
    private String statusdesc;
    private String transactionid;
    private ResponseForCountUpdate response;

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusdesc() {
        return statusdesc;
    }

    public void setStatusdesc(String statusdesc) {
        this.statusdesc = statusdesc;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public ResponseForCountUpdate getResponse() {
        return response;
    }

    public void setResponse(ResponseForCountUpdate response) {
        this.response = response;
    }
}
