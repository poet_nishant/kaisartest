package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/21/17.
 */

public class ResponseForRackGet {

    private GetRackStatusResponse getRackStatusResponse;

    public GetRackStatusResponse getGetRackStatusResponse() {
        return getRackStatusResponse;
    }

    public void setGetRackStatusResponse(GetRackStatusResponse getRackStatusResponse) {
        this.getRackStatusResponse = getRackStatusResponse;
    }
}
