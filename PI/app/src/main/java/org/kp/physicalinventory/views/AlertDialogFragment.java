package org.kp.physicalinventory.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;


public class AlertDialogFragment extends DialogFragment {

    public static String dialogText = "dialog_text",
            dialogTitle = "dialog_title",
            dialogPositiveBtnText = "pos_btn_text",
            dialogNegativeBtnText = "neg_btn_text",
            dialogCancelable = "is_cancelable";
    private IAlertDialogCallback mCallback = null;
    private IAlertDialogCancelCallback mCancelCallback = null;
    private static String TAG = AlertDialogFragment.class.getName();
    private String mDialogTag;

    public interface IAlertDialogCallback {
        void onDialogNegativeClick(String dialogTag);
        void onDialogPositiveClick(String dialogTag);
    }

    public interface IAlertDialogCancelCallback {
        void onCancelDialog(String dialogTag);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mCallback = (IAlertDialogCallback)context;
            mCancelCallback = (IAlertDialogCancelCallback) context;
        } catch(Exception e){
            Log.e(TAG,e.getMessage());
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(AlertDialogFragment.dialogTitle);
        String text = getArguments().getString(AlertDialogFragment.dialogText);
        String pos_text = getArguments().getString(AlertDialogFragment.dialogPositiveBtnText);
        String neg_text = getArguments().getString(AlertDialogFragment.dialogNegativeBtnText);
        boolean is_cancelable = getArguments().getBoolean(AlertDialogFragment.dialogCancelable,
                true);
        mDialogTag = getTag();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        if (!TextUtils.isEmpty(text)) {
            Spannable spannable = new SpannableString(text);
            spannable.setSpan(new ForegroundColorSpan(Color.GRAY), 0, text.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setMessage(spannable);
        }

        if (!TextUtils.isEmpty(neg_text)) {
            builder.setNegativeButton(neg_text,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dismiss();
                            if (mCallback != null) {
                                mCallback.onDialogNegativeClick(mDialogTag);
                            }
                        }
                    });
        }

        if (!TextUtils.isEmpty(pos_text)) {
            builder.setPositiveButton(pos_text,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dismiss();
                            if (mCallback != null) {
                                mCallback.onDialogPositiveClick(mDialogTag);
                            }
                        }
                    });
        }

        builder.setCancelable(is_cancelable);
        return  builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (mCancelCallback != null) {
            mCancelCallback.onCancelDialog(mDialogTag);
        }
    }
}

