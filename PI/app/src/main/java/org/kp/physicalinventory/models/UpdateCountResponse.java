package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class UpdateCountResponse {

    private TSSPJsonResponseForCountUpdate tsspJsonResponse;
    private static final UpdateCountResponse instance = new UpdateCountResponse();
    private UpdateCountResponse() { }
    private static UpdateCountResponse Instance;

    public TSSPJsonResponseForCountUpdate getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForCountUpdate tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }

    public static UpdateCountResponse getInstance() {
        return instance;
    }

    public static void setInstance(UpdateCountResponse instance) {
        Instance = instance;
    }
}
