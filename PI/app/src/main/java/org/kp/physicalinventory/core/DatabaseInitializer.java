package org.kp.physicalinventory.core;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import org.kp.physicalinventory.dao.NdcDao;
import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.NDC;
import org.kp.physicalinventory.views.MainActivity;
import java.util.ArrayList;
import java.util.List;

public class DatabaseInitializer {

    private static final String TAG = DatabaseInitializer.class.getName();

    public static void populateAsync(@NonNull final AppDatabase db, ArrayList<NDC> ndcList) {
        PopulateDbAsync task = new PopulateDbAsync(db, ndcList);
        task.execute();
    }

    public static void deleteAsync(@NonNull final AppDatabase db) {
        DeleteDbAsync task = new DeleteDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db, ArrayList<NDC> ndcList) {
        populateNdcData(db, ndcList);
    }

    private static void populateNdcData(AppDatabase db, ArrayList<NDC> ndcList) {
        db.ndcDao().insertList(ndcList);
        List<NDC> ndcArrayList = db.ndcDao().getAll();
        Log.d(DatabaseInitializer.TAG, "Rows Count: " + ndcArrayList.size());
    }

    private static void deleteNdcData(AppDatabase db) {
        db.ndcDao().deleteAll();
        List<NDC> ndcArrayList = db.ndcDao().getAll();
        Log.d(DatabaseInitializer.TAG, "Rows Count: " + ndcArrayList.size());
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;
        private final ArrayList<NDC> mNdcList;

        PopulateDbAsync(AppDatabase db, ArrayList<NDC> ndcList) {
            mDb = db;
            mNdcList = ndcList;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateNdcData(mDb, mNdcList);
            return null;
        }
    }

    private static class DeleteDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;


        DeleteDbAsync(AppDatabase db) {
            mDb = db;

        }

        @Override
        protected Void doInBackground(final Void... params) {
            deleteNdcData(mDb);
            return null;
        }
    }


}
