package org.kp.physicalinventory.models;

import java.util.List;

/**
 * Created by is-4586 on 11/2/17.
 */

public class GetPharmaciesResponse {

    private List<PharmacyInfo> pharmacyInfo;

    public PharmacyInfo getPharmacyInfo(int i) {
        return pharmacyInfo.get(i);
    }

    public List<PharmacyInfo> getPharmacyInfo(){
        return pharmacyInfo;
    }

    public void setPharmacyInfo(List<PharmacyInfo> pharmacyInfo) {
        this.pharmacyInfo = pharmacyInfo;
    }
}
