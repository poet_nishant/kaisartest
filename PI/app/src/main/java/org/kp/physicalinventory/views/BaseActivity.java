package org.kp.physicalinventory.views;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import org.kp.physicalinventory.controllers.LoginController;

public abstract class BaseActivity extends AppCompatActivity {

    private Handler mHandler;
    private Runnable mRunnable;
    //static int timeOut = Integer.parseInt(LoginController.getInstance().getLoginResponse().getTsspJsonResponse().getResponse().getAuthorizeUserResponse().getTimeOut());
    private static final int IDLE_TIMEOUT = 10 * 60 * 1000; // for 10 minutes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        defineTasksAfterTimeout();
    }

    abstract void logoutUserCallback();

    private void defineTasksAfterTimeout() {
        mRunnable = new Runnable() {
            @Override
            public void run() {
                stopHandler();
                logoutUserCallback();
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        startHandler();
    }

    long lastTimeStamp = 0;
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (System.currentTimeMillis() - lastTimeStamp > 5000) {
            refreshHandler();
        }
        lastTimeStamp = System.currentTimeMillis();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//
//        boolean screenOn;
//        screenOn = pm.isInteractive();
//
//        if (screenOn) {
//            // Screen is still on, so do your thing here
//        }
        stopHandler();
    }

    private void stopHandler() {
        mHandler.removeCallbacks(mRunnable);
    }

    private void startHandler() {
        mHandler.postDelayed(mRunnable, IDLE_TIMEOUT);
    }

    private void refreshHandler() {
        stopHandler();
        startHandler();
    }

}