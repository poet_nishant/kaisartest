package org.kp.physicalinventory.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestHandle;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.AlertDialogManager;
import org.kp.physicalinventory.controllers.LockableViewPager;
import org.kp.physicalinventory.controllers.LoginController;
import org.kp.physicalinventory.controllers.PharmacyListController;
import org.kp.physicalinventory.controllers.SampleFragmentPagerAdapter;
import org.kp.physicalinventory.controllers.SharedPreferencesController;
import org.kp.physicalinventory.database.AppDatabase;
import org.kp.physicalinventory.models.LoginResponse;
import org.kp.physicalinventory.models.PharmacyListResponse;

import static org.kp.physicalinventory.controllers.LoginController.LoginCallback;
import static org.kp.physicalinventory.controllers.PharmacyListController.PharmacyListFetchCallback;

public class MainActivity extends AppCompatActivity implements AlertDialogFragment
        .IAlertDialogCallback {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int LOCATION_SELECTION_REQUEST = 1;
    public static String SELECTED_STORE_ID = "store_id";
    public static String SELECTED_LOCATION_NAME = "location_name";
    RequestHandle requestHandle = null;
    SampleFragmentPagerAdapter fragmentPagerAdapter;
    SharedPreferencesController sharedPreferencesController;
    private boolean activityVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDatabase.destroyInstance(this);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null && bundle.getBoolean("IdleLogout", false)) {
            Log.d(TAG, "on create of main, if saved instance state.getboolean - idlelogout = false");
            AlertDialogManager.showIdleLogoutDialog(this);
        }


        Log.d("debug", "On create method of main activity");

        LockableViewPager viewPager = (LockableViewPager) findViewById(R.id.viewpager);

        //loadPreferences();
        sharedPreferencesController = SharedPreferencesController.getInstance(this);
        fragmentPagerAdapter = new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this);
        // Get the ViewPager and set it's PagerAdapter so that it can display items

        //ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        viewPager.setAdapter(fragmentPagerAdapter);
        viewPager.setSwipeable(false);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        tabStrip.getChildAt(1).setClickable(false);


        PharmacyListController pharmacyListController = PharmacyListController.getInstance();
        if (!pharmacyListController.hasPharmacyListResponse()) {

            PharmacyListFetchCallback callback = new PharmacyListFetchCallback() {
                @Override
                public void onPharmacyListReceived(PharmacyListResponse pharmacyListResponse) {
                    Log.d("debug", "Pharmacy list successfull callback");
                    requestHandle = null;
                    runOnUiThread(() -> Log.d("debug", "Before call to show success dialog"));
                }

                @Override
                public void onPharmacyListFetchFailed() {
                    requestHandle = null;
                    runOnUiThread(() -> showFailureDialog());
                }

                public void onPharmacyListFetchDbDown() {
                    requestHandle = null;
                    runOnUiThread(() -> showDialogForDbAccessFailed());
                }

                public void onPharmacyListRequestInvalid() {
                    requestHandle = null;
                    runOnUiThread(() -> showDialogForInvalidRequest());
                }

                public void onPharmacyListRequestTimeout() {
                    requestHandle = null;
                    Log.d(TAG, "Inside pharmacylistrequesttimeout");
                    runOnUiThread(() -> showFailureDialogForRequestTimeout());
                }
            };

            Log.d("debug", "Calling method get pharmacy list");
            requestHandle = pharmacyListController.getPharmacyList(callback);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityVisible = false;
        Log.d(TAG, "On pause of Main Activity");
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "On destroy of main activity");
        super.onDestroy();

        if (requestHandle != null) {
            requestHandle.cancel(true);
        }
    }

    private void showFailureDialogForRequestTimeout() {
        if (activityVisible) {
            AlertDialogManager.showUnableToConnectDialog2(this);
        }
    }

    private void showFailureDialog() {
        if (activityVisible) {
            AlertDialogManager.showUnableToConnectDialog2(this);
        }
    }

    private void showDialogForAccountLocked() {
        if (activityVisible) {
            AlertDialogManager.showAccountLockedDialog(this);
        }
    }

    private void showDialogForUserNotFound() {
        if (activityVisible) {
            AlertDialogManager.showUserNotFoundDialog(this);
        }
    }

    private void showDialogForUserUnauthorized() {
        if (activityVisible) {
            AlertDialogManager.showUserUnauthorizedDialog(this);
        }
    }

    private void showDialogForDbAccessFailed() {
        if (activityVisible) {
            AlertDialogManager.showUnableToConnectDialog(this);
        }
    }

    private void showDialogForInvalidRequest() {
        if (activityVisible) {
            AlertDialogManager.showInvalidRequestDialog(this);
        }
    }

    public void loginForPhysicalInventory(View view) {

        LoginController loginController = LoginController.getInstance();

        Button login = (Button) findViewById(R.id.login);
        login.setClickable(false);
        PageFragment_pi fragment_pi = (PageFragment_pi) fragmentPagerAdapter.getFragment(
                SampleFragmentPagerAdapter.FragmentType.PHYSICAL_INVENTORY);
        fragment_pi.clearErrorMessages();


        if (!loginController.hasLoginResponse()) {

            LoginCallback callback = new LoginCallback() {
                @Override
                public void onLoginPassed(LoginResponse loginResponse) {

                    // Handle scenario for status code ! = 000)
                    Log.d("debug", "Login successfull callback");
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EditText username = (EditText) findViewById(R.id.username_pi);

                            EditText password = (EditText) findViewById(R.id.password_pi);


                            Intent intent = new Intent(getApplicationContext(), RackViewActivity
                                    .class);

                            startActivity(intent);
                            username.getText().clear();
                            password.getText().clear();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);

                        }
                    });
                }

                @Override
                public void onLoginFailed() {
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //LoginController.getInstance().clearLoginResponse();
                            showFailureDialog();
                            LoginController.getInstance().clearLoginResponse();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);

                        }
                    });
                }

                public void onUserAccountLocked() {
                    requestHandle = null;
                    runOnUiThread(() -> {
                        //LoginController.getInstance().clearLoginResponse();
                        showDialogForAccountLocked();
                        LoginController.getInstance().clearLoginResponse();
                        Button login = (Button) findViewById(R.id.login);
                        login.setClickable(true);

                    });
                }

                public void onUserNotFound() {
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //LoginController.getInstance().clearLoginResponse();
                            showDialogForUserNotFound();
                            LoginController.getInstance().clearLoginResponse();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);

                        }
                    });
                }

                public void onUserUnauthorized() {
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //LoginController.getInstance().clearLoginResponse();
                            showDialogForUserUnauthorized();
                            LoginController.getInstance().clearLoginResponse();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);

                        }
                    });
                }

                public void onLoginRequestTimeout() {
                    requestHandle = null;
                    runOnUiThread(() -> {
                        //LoginController.getInstance().clearLoginResponse();
                        Log.d("TAG", "on login request timeout");
                        showFailureDialogForRequestTimeout();
                        LoginController.getInstance().clearLoginResponse();
                        Button login12 = (Button) findViewById(R.id.login);
                        login12.setClickable(true);


                    });
                }

                public void onLoginCredentialsWrong() {
                    Log.d(TAG, "Login credentials wrong callback");

                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView ErrorMsgBelowUsername = (TextView) findViewById(R.id
                                    .invalid_credentials_below_username);
                            TextView ErrorMsgBelowPassword = (TextView) findViewById(R.id
                                    .invalid_credentials_below_password);
                            ErrorMsgBelowUsername.setVisibility(View.VISIBLE);
                            ErrorMsgBelowPassword.setVisibility(View.VISIBLE);
                            LinearLayout usernameLinearLayout = (LinearLayout) findViewById(R.id
                                    .username_linearlayout);
                            LinearLayout passwordLinearLayout = (LinearLayout) findViewById(R.id
                                    .password_linearlayout);
                            usernameLinearLayout.setBackground(getDrawable(R.drawable
                                    .linear_layout_shape_error));
                            passwordLinearLayout.setBackground(getDrawable(R.drawable
                                    .linear_layout_shape_error));
                            Log.d(TAG, "Deleting login response");
                            LoginController.getInstance().clearLoginResponse();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);

                        }
                    });
                }

                public void onLoginDbAccessFailed() {
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogForDbAccessFailed();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);
                        }
                    });
                }

                public void onLoginRequestInvalid() {
                    requestHandle = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogForInvalidRequest();
                            Button login = (Button) findViewById(R.id.login);
                            login.setClickable(true);
                        }
                    });
                }
            };

            Log.d("debug", "Calling method login using ID and password");
            EditText username = (EditText) findViewById(R.id.username_pi);
            Log.d(TAG, "find view by id username");

            EditText password = (EditText) findViewById(R.id.password_pi);
            Log.d(TAG, "find view by id password");

            requestHandle = loginController.loginUsingIDandPwd(username.getText().toString(),
                    password.getText().toString(),
                    sharedPreferencesController.loadSelectedPharmacyStoreId(), sharedPreferencesController.loadSelectedPharmacyName(),
                    callback);
        }
    }

    public void loginForCycleCount(View view) {

    }

    public void onLocationSelectorClicked(View view) {
        Intent selectLocationIntent = new Intent(this, LocationSelectorActivity.class);
        startActivityForResult(selectLocationIntent, LOCATION_SELECTION_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == LOCATION_SELECTION_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String selectedPharmacyName = data.getStringExtra(SELECTED_LOCATION_NAME);
                String selectedStoreId = data.getStringExtra(SELECTED_STORE_ID);
                Log.d(TAG, "Selected pharmacy name:" + selectedPharmacyName);
                Log.d(TAG, "Selected store id:" + selectedStoreId);
                setPharmacyNameAndStoreId(selectedPharmacyName, selectedStoreId);
            }
        }
    }

    private void setPharmacyNameAndStoreId(String pharmacyName, String storeId) {
        PageFragment_pi fragment_pi = (PageFragment_pi) fragmentPagerAdapter.getFragment(
                SampleFragmentPagerAdapter.FragmentType.PHYSICAL_INVENTORY);
        fragment_pi.setPharmacyLocationNameAndStoreId(pharmacyName, storeId);
        sharedPreferencesController.savePharmacyNameAndStoreId(pharmacyName, storeId);
    }

    @Override
    public void onResume() {
        super.onResume();
        activityVisible = true;
        Log.d(TAG, "On resume of Main Activity");
    }

    @Override
    public void onDialogNegativeClick(String dialogTag) {
        switch (dialogTag) {
            case AlertDialogManager.UNABLE_TO_CONNECT_DIALOG_TAG:
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onDialogPositiveClick(String dialogTag) {

    }
}
