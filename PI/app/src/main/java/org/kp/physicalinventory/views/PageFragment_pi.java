package org.kp.physicalinventory.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.kp.physicalinventory.R;
import org.kp.physicalinventory.controllers.LoginController;
import org.kp.physicalinventory.controllers.SharedPreferencesController;

/**
 * Created by is-4586 on 11/1/17.
 */

// In this case, the fragment displays simple text based on the page
public class PageFragment_pi extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String TAG = MainActivity.class.getSimpleName();
    private int mPage;
    private TextView pharmacyName;
    private TextView ErrorMsgBelowUsername;
    private TextView ErrorMsgBelowPassword;
    private LinearLayout usernameLinearLayout;
    private LinearLayout passwordLinearLayout;

    SharedPreferencesController sharedPreferencesController;


    public static PageFragment_pi newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment_pi fragment = new PageFragment_pi();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPage = getArguments().getInt(ARG_PAGE);

    }

    public void onPause(){
        super.onPause();
        Log.d(TAG, "On Pause of PI fragment");
    }

    public void onResume(){
        super.onResume();
        Log.d(TAG, "On Resume of PI Fragment");
        LoginController.getInstance().clearLoginResponse();
    }

    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "On destroy of PI Fragment");
    }

    @Override
    public RelativeLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sharedPreferencesController = SharedPreferencesController.getInstance(getContext());
        View view = inflater.inflate(R.layout.fragment_page_pi, container, false);
        //TextView textView = (TextView) view;
        //textView.setText("Fragment #" + mPage);
        //return view;
        RelativeLayout relativeLayout = (RelativeLayout) view;

        usernameLinearLayout = (LinearLayout) relativeLayout.findViewById(R.id
                .username_linearlayout);
        passwordLinearLayout = (LinearLayout) relativeLayout.findViewById(R.id
                .password_linearlayout);

        ErrorMsgBelowUsername = (TextView) relativeLayout.findViewById(R.id.invalid_credentials_below_username);
        ErrorMsgBelowPassword = (TextView) relativeLayout.findViewById(R.id.invalid_credentials_below_password);
        TextView ErrorMsgBelowLocation = (TextView) relativeLayout.findViewById(R.id.pharmacy_selection_error);

        ErrorMsgBelowUsername.setVisibility(View.INVISIBLE);
        ErrorMsgBelowPassword.setVisibility(View.INVISIBLE);
        ErrorMsgBelowLocation.setVisibility(View.INVISIBLE);

        pharmacyName = (TextView) relativeLayout.findViewById(R.id.selected_pharmacy_name);
        setPharmacyLocationNameAndStoreId(sharedPreferencesController.loadSelectedPharmacyName(),
                sharedPreferencesController.loadSelectedPharmacyStoreId());
        return relativeLayout;
    }

    public void setPharmacyLocationNameAndStoreId(String name, String storeId) {
        if("".equals(name) || name == null) {
            pharmacyName.setHint(getString(R.string.select_pharmacy_location));
        } else {
            pharmacyName.setText(getString(R.string.pharmacy_name_and_store_id, name, storeId));
        }
    }


    public void clearErrorMessages() {

        usernameLinearLayout.setBackground(getContext().getDrawable(R.drawable
                .linear_layout_shape));
        passwordLinearLayout.setBackground(getContext().getDrawable(R.drawable
                .linear_layout_shape));
        ErrorMsgBelowUsername.setVisibility(View.INVISIBLE);
        ErrorMsgBelowPassword.setVisibility(View.INVISIBLE);
    }
}