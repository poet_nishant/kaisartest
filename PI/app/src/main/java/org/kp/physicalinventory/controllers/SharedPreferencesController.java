package org.kp.physicalinventory.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.kp.physicalinventory.R;

/**
 * Created by is-4586 on 11/18/17.
 */

public class SharedPreferencesController {

    private static final String TAG = SharedPreferencesController.class.getSimpleName();
    private static final String PREFS_NAME = "preferences";
    private static final String PREF_PNAME = "Pharmacy";
    private static final String PREF_STORE_ID = "StoreID";
    private final String DEFAULT_SELECTED_PHARMACY_NAME = "";
    private final String DEFAULT_STORE_ID = "";

    private SharedPreferences preferences;

    private static SharedPreferencesController instance = null;

    private SharedPreferencesController(Context context){
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferencesController getInstance(Context context) {
        if(instance == null) {
            instance = new SharedPreferencesController(context);
        }
        return instance;
    }

    public void savePharmacyNameAndStoreId(String selectedPharmacyName, String selectedStoreId) {
        SharedPreferences.Editor editor = preferences.edit();
        Log.d(TAG, "Inside save preferences");
        Log.d(TAG, selectedPharmacyName);
        //System.out.println("onPause save store:" + selectedPharmacyName);
        editor.putString(PREF_PNAME, selectedPharmacyName);
        editor.putString(PREF_STORE_ID, selectedStoreId);
        editor.apply();
    }

    public String loadSelectedPharmacyName() {
        // Get value
        Log.d(TAG, "Returning selected pharmacy Name");
        return preferences.getString(PREF_PNAME, DEFAULT_SELECTED_PHARMACY_NAME);
    }

    public String loadSelectedPharmacyStoreId() {
        Log.d(TAG, "Returning selected pharmacy StoreId");
        return preferences.getString(PREF_STORE_ID, DEFAULT_STORE_ID);
    }

}
