package org.kp.physicalinventory.models;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/2/17.
 */

public class GetPINDCListResponse {

    private String phmNo;
    private ArrayList<NDC> piNDC;

    public String getPhmNo() {
        return phmNo;
    }

    public void setPhmNo(String phmNo) {
        this.phmNo = phmNo;
    }

    public ArrayList<NDC> getPiNDC() {
        return piNDC;
    }

    public NDC getNdc(int i) { return piNDC.get(i);}

    public void setPiNDC(ArrayList<NDC> piNDC) {
        this.piNDC = piNDC;
    }

}
