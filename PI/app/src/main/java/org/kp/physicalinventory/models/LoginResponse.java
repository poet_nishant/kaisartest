package org.kp.physicalinventory.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by is-4586 on 11/1/17.
 */

public class LoginResponse {

    @SerializedName("TSSPJsonResponse")
    private TSSPJsonResponseForLogin tsspJsonResponse;

    public TSSPJsonResponseForLogin getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForLogin tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }
}





