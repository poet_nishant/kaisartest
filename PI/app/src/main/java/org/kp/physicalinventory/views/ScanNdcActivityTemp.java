package org.kp.physicalinventory.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toolbar;

import org.kp.physicalinventory.R;

public class ScanNdcActivityTemp extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    //    RequestHandle requestHandle = null;
    private String storeId;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_ndc_temp);

        Intent i = getIntent();

//        String rack_num_selected = i.getStringExtra("rackNum");
        //rack_num_selected = "11111";

//        storeId = i.getStringExtra("storeId");
        //storeId = "900003006";

//        username = i.getStringExtra("username");
        //username = "W081411";

        //setSupportActionBar(toolbar);
//        setTitle("        Scan NDC for Rack " + rack_num_selected);
//        TextView text = (TextView) findViewById(R.id.scan_rack_selected);
//        text.setText("The selected rack is: " + rack_num_selected);
//        Log.d(TAG, "on scanndcactivity" + rack_num_selected);

        TextView drug_name = (TextView) findViewById(R.id.drug_name);

        String drugName = i.getStringExtra("selectedNdc");
        drug_name.setText(getString(R.string.drug_scanned_is, drugName));
    }
}
