package org.kp.physicalinventory.core;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;

import java.text.NumberFormat;

/**
 * Created by bhavya.mehta on 22-11-2017.
 */

public class TextUtility {

    public static String longToStr(long longValue) {
        return String.valueOf(longValue);
    }

    public static String floatToStr(float floatValue) {
        return String.valueOf(floatValue);
    }
    public static String intToStr(int intValue) {
        return String.valueOf(intValue);
    }



    public static Spannable makeBold(String str) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);
        setBoldSpan(sb, str);
        return sb;
    }

    public static Spannable SetDimension(Context context, String str, int textSize) {
        return setSize(str, context.getResources().getDimensionPixelSize(textSize));
    }

    public static Spannable makeBoldAndSetDimension(Context context, String str, int textSize) {
        return makeBoldAndSetSize(str, context.getResources().getDimensionPixelSize(textSize));
    }

    public static Spannable makeNormalAndSetDimension(Context context, String str, int textSize) {
        return makeNormalAndSetSize(str, context.getResources().getDimensionPixelSize(textSize));
    }

    private static Spannable setSize(String str, int textSize) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);
        setAbsoluteSizeSpan(sb, str, textSize);
        return sb;
    }

    private static void setAbsoluteSizeSpan(SpannableStringBuilder sb, String str, int textSize) {
        sb.setSpan(new AbsoluteSizeSpan(textSize), 0, str.length(),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE);
    }

    private static void setBoldSpan(SpannableStringBuilder sb, String str) {
        final StyleSpan bss = new StyleSpan(Typeface.BOLD);
        sb.setSpan(bss, 0, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
    }

    private static void setNormalSpan(SpannableStringBuilder sb, String str) {
        final StyleSpan bss = new StyleSpan(Typeface.NORMAL);
        sb.setSpan(bss, 0, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
    }

    private static Spannable makeBoldAndSetSize(String str, int textSize) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);
        setBoldSpan(sb, str);
        setAbsoluteSizeSpan(sb, str, textSize);
        return sb;
    }
    private static Spannable makeNormalAndSetSize(String str, int textSize) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);
        setNormalSpan(sb, str);
        setAbsoluteSizeSpan(sb, str, textSize);
        return sb;
    }


    public static long strToLong(String text) {
        try {
            return Long.parseLong(text);
        } catch (Exception e) {
            return -1;
        }
    }

    public static float getFloat(String str) {
        float res = 0.0f;
        try {
            res = Float.valueOf(str);
        } catch(NumberFormatException e) {
            res = 0.0f;
        }
        return res;
    }

    public static boolean isDigit(CharSequence text) {
        String regex = "[0-9]+";
        return (text.toString().matches(regex));
    }
}
