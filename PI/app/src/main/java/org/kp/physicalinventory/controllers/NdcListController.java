package org.kp.physicalinventory.controllers;

/**
 * Created by is-4586 on 11/2/17.
 */

import android.util.Log;

import java.util.ArrayList;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.NDCListResponse;
import org.kp.physicalinventory.models.NdcListRequest;
import org.kp.physicalinventory.models.NDC;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class NdcListController {

    private static NdcListController instance = null;
    public static NdcListController getInstance() {
        if(instance == null) {
            instance = new NdcListController();
        }
        return instance;
    }

    private NdcListController() {}

    private NDCListResponse ndcListResponse;


    public interface NdcListFetchCallback {
        void onNdcListReceived(NDCListResponse ndcListResponse);
        void onNdcListFetchFailed();
        void onNdcListRequestTimeout();
    }

    public boolean hasNdcListResponse() {
        return ndcListResponse != null;
    }

    public RequestHandle getNdcList(String storeId, String username, Boolean varianceInclude, final NdcListFetchCallback callback){

        final NdcListRequest ndcListRequest = new NdcListRequest();

        ndcListRequest.setDeviceid("");
        ndcListRequest.setAppver(null);
        ndcListRequest.setAppid(null);
        ndcListRequest.setAction("getPINDCList");
        ndcListRequest.setMode("PI");
        ndcListRequest.setRegion("CO");
        ndcListRequest.setStoreid("9000" + storeId);
        ndcListRequest.setUserid(username);
        ndcListRequest.setTransactionid(null);
        ndcListRequest.setTimeZone(TimeZoneController.getTimeZone());
        ndcListRequest.getAssignedNdcRequest().setIncludeVariances(varianceInclude);

        Log.d("TAG", "getNDCList");

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(NdcListController.class.getSimpleName(), "On success of ndc list get");
                Log.d(NdcListController.class.getSimpleName(), "response of PINDCList : " + response );
                String httpResponseBody = response.toString();
                ndcListResponse = JsonParser.ParseJsonForNdcList(httpResponseBody);
                callback.onNdcListReceived(ndcListResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {


                Log.d("TAG", "On pharmacy list request timeout");
                callback.onNdcListRequestTimeout();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("TAG", "on failure of ndc list get");
                callback.onNdcListFetchFailed();

            }
        };
        RequestHandle requestHandle = HttpUtil.httpCall(ndcListRequest, responseHandler);

        return requestHandle;
    }

    public int PendingDrugCount(){
        return ndcListResponse.getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC().size();
    }

    public ArrayList<NDC> getPendingNdcList(){
        if (PendingDrugCount() > 0){
            return ndcListResponse.getTSSPJsonResponse().getResponse().getGetPINDCListResponse().getPiNDC();
        }
        else{
            return null;
        }
    }

}
