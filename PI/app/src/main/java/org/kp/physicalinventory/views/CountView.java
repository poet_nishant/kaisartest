package org.kp.physicalinventory.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.core.TextUtility;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static android.text.InputType.TYPE_CLASS_NUMBER;

public class CountView extends LinearLayout {

    @BindView(R.id.view_count_delete)
    ImageView mDelete;
    @BindView(R.id.view_count_plus)
    ImageView mPlus;
    @BindView(R.id.view_count_minus)
    ImageView mMinus;
    @BindView(R.id.view_count_text)
    EditText mText;
    private ICountChange mCountChange;
    private float mCountValue;
    private boolean mIsDeleteVisible;
    private boolean mIsPartial;
    private ICountDeleted mCountDeleted;
    private Boolean isFloat = false;
    private Boolean isFullPack = false;

    public CountView(Context context) {
        super(context);
        init();
    }

    public CountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MyCountView);
        mIsDeleteVisible = a.getBoolean(R.styleable.MyCountView_isDeleteVisible, false);
        mIsPartial = a.getBoolean(R.styleable.MyCountView_isPartial,false);
        init();
        a.recycle();


    }

    public CountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MyCountView,defStyleAttr,0);
        mIsDeleteVisible = a.getBoolean(R.styleable.MyCountView_isDeleteVisible, false);
        mIsPartial = a.getBoolean(R.styleable.MyCountView_isPartial,false);
        init();
        a.recycle();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        inflate(getContext(), R.layout.view_count, this);
        ButterKnife.bind(this);
        deleteVisibility();
        setInputBox();
    }



    private float getCount(String text) {
        if (TextUtils.isEmpty(text)) {
            text = "0";
        }

        return TextUtility.getFloat(text);
    }

    @OnTextChanged(value = R.id.view_count_text,
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterTextChange(CharSequence text) {
        mCountValue = getCount(text.toString());
        if (mCountChange != null && mCountValue >= 0) {
            mCountChange.getCount(mCountValue, this);
        }
    }

    private float roundOneDecimal(float d)
    {
        DecimalFormat oneDForm = new DecimalFormat("#.#");
        return Float.valueOf(oneDForm.format(d));
    }

    @OnClick(R.id.view_count_plus)
    public void plusClicked() {
        if (mCountValue >= 0) {
            if (isFloat) {
                if (isFullPack){
                    mCountValue ++;
                }else {
                    mCountValue = roundOneDecimal(mCountValue += 0.1);
                }

            } else{
                mCountValue++;
            }

            mText.setText(TextUtility.floatToStr(mCountValue));
        }
    }

    @OnClick(R.id.view_count_minus)
    public void minusClicked() {
        if (mCountValue > 0) {
            if (isFloat){
                if (isFullPack){
                    mCountValue--;
                }
                else {
                    //mCountValue = Math.round(mCountValue -= 0.1);
                    mCountValue = roundOneDecimal(mCountValue -= 0.1);
                }

            } else{
                mCountValue--;
            }
            if (mCountValue >= 0) {
                mText.setText(TextUtility.floatToStr(mCountValue));
            }
        }
    }

    public void setCount(Float count){
        mText.setText(TextUtility.floatToStr(count));
    }

    @OnClick(R.id.view_count_delete)
    public void deleteClicked() {
        clearText();
        if(mCountDeleted != null) {
            mCountDeleted.onDeleted();
        }
    }

    public void clearText() {
        mText.getText().clear();
    }

    public void addCountChangeListener(ICountChange iCountChange, Boolean fullPack) {

        mCountChange = iCountChange;
        if (fullPack){
            isFullPack = true;
        }
    }

    public void addCountDeletedListner(ICountDeleted iCountDeleted) {
        mCountDeleted = iCountDeleted;
    }

    public void setDeleteVisible(boolean flag) {
        mIsDeleteVisible = flag;
        deleteVisibility();
    }

    private void deleteVisibility() {
        if (mIsDeleteVisible) {
            mDelete.setVisibility(VISIBLE);
        } else {
            mDelete.setVisibility(GONE);
        }
    }

    private void setInputBox() {
        if(mIsPartial) {
            //mText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_CLASS_NUMBER);
            mText.setInputType(InputType.TYPE_CLASS_NUMBER);
            Log.d("test" ,"partial"+mIsPartial);
        }else{
            mText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
    }

    public void setInputTypeToFloat() {
        isFloat = true;
        mText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL|TYPE_CLASS_NUMBER);
    }

    public void setInputTypeToNumber() {
        mText.setInputType(TYPE_CLASS_NUMBER);
    }

    interface ICountChange {
        void getCount(float count, CountView view);
    }

    interface ICountDeleted {
        void onDeleted();
    }
}
