package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/21/17.
 */

public class ResponseForRackUpdate {

    private String updateRackStatusResponse;

    public String getUpdateRackStatusResponse() {
        return updateRackStatusResponse;
    }

    public void setUpdateRackstatusResponse(String updateRackstatusResponse) {
        this.updateRackStatusResponse = updateRackStatusResponse;
    }
}
