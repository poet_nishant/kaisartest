package org.kp.physicalinventory.controllers;

import android.util.Log;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.LoginRequest;
import org.kp.physicalinventory.models.LoginResponse;

import java.net.SocketTimeoutException;
import java.util.Objects;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by is-4586 on 11/2/17.
 */

public class LoginController{

    private static LoginController instance = null;
    public static LoginController getInstance() {
        if(instance == null) {
            instance = new LoginController();
        }
        return instance;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public String getUsername() {
        return username;
    }

    public String getStoreName() { return storeName; }

    private String storeNumber;
    private String username;
    private String storeName;

    private LoginController() {}

    private LoginResponse loginResponse;

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public void clearLoginResponse(){
        loginResponse = null;
        Log.d("LoginController", "Clearing login response");
    }

    public interface LoginCallback {
        void onLoginPassed(LoginResponse loginResponse);
        void onLoginFailed();
        void onLoginCredentialsWrong();
        void onLoginDbAccessFailed();
        void onLoginRequestInvalid();
        void onUserAccountLocked();
        void onLoginRequestTimeout();
        void onUserNotFound();
        void onUserUnauthorized();
    }

    public boolean hasLoginResponse() {
        return loginResponse != null;
    }

    public RequestHandle loginUsingIDandPwd(String userid, String password, String storeId, String storename, final LoginController.LoginCallback callback){

        final LoginRequest loginRequest = new LoginRequest();

        Log.d("debug", "Login request instance");

        loginRequest.setDeviceid("");
        loginRequest.setAppver(null);
        loginRequest.setAppid(null);
        loginRequest.setAction("piAuthorizeUser");
        loginRequest.setMode(null);
        loginRequest.setRegion(null);
        loginRequest.setStoreid("9000" + storeId);
        loginRequest.setUserid(userid);
        loginRequest.setPassword(password);
        loginRequest.setTransactionid(null);


        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                username = userid;
                storeNumber = storeId;
                storeName = storename;

                String httpResponseBody = response.toString();
                Log.d("debug", "The response body is: " + httpResponseBody);
                loginResponse = (JsonParser.ParseJsonForLogin(httpResponseBody));
                if ("000".equals(loginResponse.getTsspJsonResponse().getStatuscode())) {
                    Log.d("debug", "Login response object created by casting parsed json");
                    callback.onLoginPassed(loginResponse);
                }
               else if("010".equals(loginResponse.getTsspJsonResponse().getStatuscode())){
                    if ("User Account Locked".equals(loginResponse.getTsspJsonResponse().getStatusdesc())){
                        callback.onUserAccountLocked();
                        return;
                    }
                    else if("User not found in LDAP".equals(loginResponse.getTsspJsonResponse().getStatusdesc())){
                        callback.onUserNotFound();
                    }

                    else if("User Unauthorized".equals(loginResponse.getTsspJsonResponse().getStatusdesc())){
                        callback.onUserUnauthorized();
                    }

                    else {
                        callback.onLoginCredentialsWrong();
                    }
                }
                else if("020".equals(loginResponse.getTsspJsonResponse().getStatuscode())){
                    callback.onLoginDbAccessFailed();
                }
                else if("400".equals(loginResponse.getTsspJsonResponse().getStatuscode())){
                    callback.onLoginRequestInvalid();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {

                    callback.onLoginRequestTimeout();
                    Log.d("TAG", "On pharmacy list request timeout");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onLoginDbAccessFailed();
            }

        };
        RequestHandle requestHandle = HttpUtil.httpCall(loginRequest, responseHandler);

        return requestHandle;
    }
}
