package org.kp.physicalinventory.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import org.kp.physicalinventory.R;

/**
 * Created by is-4586 on 11/1/17.
 */

// In this case, the fragment displays simple text based on the page
public class PageFragment_cc extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;

    public static PageFragment_cc newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment_cc fragment = new PageFragment_cc();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public RelativeLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page_cc, container, false);
        //TextView textView = (TextView) view;
        //textView.setText("Fragment #" + mPage);
        //return view;

        RelativeLayout relativeLayout = (RelativeLayout) view;
        return relativeLayout;
    }
}