package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class ResponseForLogin {

    private AuthorizeUserResponse authorizeUserResponse;

    public AuthorizeUserResponse getAuthorizeUserResponse() {
        return authorizeUserResponse;
    }

    public void setAuthorizeUserResponse(AuthorizeUserResponse authorizeUserResponse) {
        this.authorizeUserResponse = authorizeUserResponse;
    }
}
