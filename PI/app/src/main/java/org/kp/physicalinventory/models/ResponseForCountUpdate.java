package org.kp.physicalinventory.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class ResponseForCountUpdate {

    private String updatePINDCResponse;

    public String getUpdatePINDCResponse() {
        return updatePINDCResponse;
    }

    public void setUpdatePINDCResponse(String updatePINDCResponse) {
        this.updatePINDCResponse = updatePINDCResponse;
    }
}
