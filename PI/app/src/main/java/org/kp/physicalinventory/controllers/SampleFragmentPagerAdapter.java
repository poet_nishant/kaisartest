package org.kp.physicalinventory.controllers;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Button;

import org.kp.physicalinventory.R;
import org.kp.physicalinventory.views.PageFragment_pi;
import org.kp.physicalinventory.views.PageFragment_cc;

/**
 * Created by is-4586 on 11/8/17.
 */

public class SampleFragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Physical Inventory", "Cycle Count" };
    private Context context;
    private FragmentManager fragmentManager;

    public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1){
            return PageFragment_cc.newInstance(position + 1);
        } else {
            return PageFragment_pi.newInstance(position + 1);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    public enum FragmentType {
        PHYSICAL_INVENTORY,
        CYCLE_COUNT
    }
    public Fragment getFragment(FragmentType type)
    {
        int fragmentPosition = type.ordinal();
        return fragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + fragmentPosition);
    }
}



