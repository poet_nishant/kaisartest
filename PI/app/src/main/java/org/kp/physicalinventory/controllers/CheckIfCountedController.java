package org.kp.physicalinventory.controllers;

/**
 * Created by is-4586 on 11/2/17.
 */

import android.util.Log;

import java.util.ArrayList;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import org.json.JSONObject;
import org.kp.physicalinventory.core.HttpUtil;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.CheckIfCountedResponse;
import org.kp.physicalinventory.models.CheckIfCountedRequest;
import org.kp.physicalinventory.models.NDC;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class CheckIfCountedController {

    private static final String TAG = CheckIfCountedController.class.getSimpleName();
    private static CheckIfCountedController instance = null;
    public static CheckIfCountedController getInstance() {
        if(instance == null) {
            instance = new CheckIfCountedController();
        }
        return instance;
    }

    private CheckIfCountedController() {}

    private CheckIfCountedResponse checkIfCountedResponse;


    public interface CheckIfCountedCallback {
        void onPassed(CheckIfCountedResponse checkIfCountedResponse);
        void onDbAccessFailed();
        void onRequestTimeout();
    }

    public boolean hasCheckIfCountedResponse() {
        return checkIfCountedResponse != null;
    }

    public RequestHandle checkIfAlreadyCounted(String storeId, String userId, String ndc, final CheckIfCountedCallback callback){

        final CheckIfCountedRequest checkIfCountedRequest = new CheckIfCountedRequest();

        checkIfCountedRequest.setDeviceid(null);
        checkIfCountedRequest.setAppver(null);
        checkIfCountedRequest.setAppid(null);
        checkIfCountedRequest.setAction("checkCountedPINDC");
        checkIfCountedRequest.setMode(null);
        checkIfCountedRequest.setRegion(null);
        checkIfCountedRequest.setStoreid("9000" + storeId);
        checkIfCountedRequest.setUserid(userId);
        checkIfCountedRequest.setTransactionid(null);
        checkIfCountedRequest.setTimeZone(TimeZoneController.getTimeZone());
        checkIfCountedRequest.getCheckCountedPINDCRequest().setNdc(ndc);

        Log.d("TAG", "getNDCList");

        //HttpUtil call, parse, return true if statuscode == 000
        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String httpResponseBody = response.toString();
                Log.d(TAG, "CheckIfCounted Response:\n" + httpResponseBody);
                checkIfCountedResponse = JsonParser.ParseJsonForCheckIfCounted(httpResponseBody);
                callback.onPassed(checkIfCountedResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable t, JSONObject json) {
                Log.d(TAG, "CheckIfCounted Response Failure");

                if (t.getCause() instanceof ConnectTimeoutException) {
                    callback.onRequestTimeout();

                }
                else{
                    callback.onDbAccessFailed();
                }
            }
        };

        return HttpUtil.httpCall(checkIfCountedRequest, responseHandler);
    }
}
