package org.kp.physicalinventory.controllers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.kp.physicalinventory.models.RackInfo;
import org.kp.physicalinventory.R;
import java.util.ArrayList;

/**
 * Created by is-4586 on 11/21/17.
 */

public class RackStatusAdapterWithGesture extends ArrayAdapter<RackInfo> {

    private RackCompleteCountCallback callback;
    private static final String TAG = RackStatusAdapter.class.getSimpleName();

    public interface RackCompleteCountCallback {
        void onCompleteCountClicked(RackInfo rackInfo);
    }

    public RackStatusAdapterWithGesture(Context context, ArrayList<RackInfo> racks, RackCompleteCountCallback callback) {
        super(context, 0, racks);
        this.callback = callback;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        RackInfo rackInfo = getItem(position);

        //Log.d("getView", rackInfo.getRackNo());
        // Check if an existing view is being reused, otherwise inflate the view
        Log.d(TAG,"index:" + position + " convertView" + convertView);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_rack, parent, false);
        }
        // Log.d("convertViewNull", "inflate item_rack");

//        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_rack, parent, false);

        // Lookup view for data population
        TextView rack_Num = (TextView) convertView.findViewById(R.id.rack_num);
        TextView rack_Status = (TextView) convertView.findViewById(R.id.rack_status);
        // Populate the data into the template view using the data object
        rack_Num.setText(rackInfo.getRackNo());
        //rack_Status.setText(rackInfo.getRackStatus());
        final RelativeLayout slideableRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.rack_item_slideable_view);
        ImageButton imageButton = (ImageButton) convertView.findViewById(R.id.rack_item_slide_hint);
//        imageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(slideableRelativeLayout.getX() == 0) {
//                    slideableRelativeLayout.animate().translationXBy(-
//                            getContext().getResources().getDimensionPixelOffset(R.dimen.rack_item_hint_width));
//                } else {
//                    slideableRelativeLayout.animate().translationXBy(
//                            getContext().getResources().getDimensionPixelOffset(R.dimen.rack_item_hint_width));
//                }
//            }
//        });

        RelativeLayout itemLayout = (RelativeLayout) convertView.findViewById(R.id.rack_item_slideable_view);
        itemLayout.setOnTouchListener(new OnSwipeTouchListener(this.getContext()) {
            @Override
            public void onSwipeDown() {
                Log.d("gesture", "Swipe down");
            }

            @Override
            public void onSwipeLeft() {
                if(slideableRelativeLayout.getX() == 0) {
                    slideableRelativeLayout.animate().translationXBy(-
                            getContext().getResources().getDimensionPixelOffset(R.dimen.rack_item_hint_width));
                } else {
                    slideableRelativeLayout.animate().translationXBy(
                            getContext().getResources().getDimensionPixelOffset(R.dimen.rack_item_hint_width));
                }
            }

            @Override
            public void onSwipeUp() {
                Log.d("gesture", "Swipe up");
            }

            @Override
            public void onSwipeRight() {
                Log.d("gesture", "Swipe right");
            }

        });

        RelativeLayout completeCountView = (RelativeLayout) convertView.findViewById(R.id.complete_count_view);
        completeCountView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                callback.onCompleteCountClicked(getItem(position));
            }
        });
        // Return the completed view to render on screen
        Log.d(TAG,"Rack Status:" + rackInfo.getRackStatus());
        if("COMPLETED".equals(rackInfo.getRackStatus())) {
            imageButton.setVisibility(View.INVISIBLE);
            imageButton.setOnClickListener(null);
            rack_Status.setBackground(getContext().getDrawable(R.drawable.shape_rectange_green));
            rack_Status.setText("Completed");
        } else {
            imageButton.setVisibility(View.VISIBLE);
            rack_Status.setBackground(getContext().getDrawable(R.drawable.shape_rectange_orange));
            rack_Status.setText("In progress");
        }
        return convertView;
    }
}
