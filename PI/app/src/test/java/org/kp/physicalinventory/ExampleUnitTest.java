package org.kp.physicalinventory;

import com.google.gson.Gson;

import junit.framework.Assert;

import org.junit.Test;
import org.kp.physicalinventory.core.JsonParser;
import org.kp.physicalinventory.models.LoginResponse;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    public static final String INVALID_LOGIN_RESPONSE = "{\"TSSPJsonResponse\":{\"statuscode\":\"010\",\"statusdesc\":\"User Nuid Cannot Be Empty\\/Blank.\",\"transactionId\":\"c9da7ba5-c2ea-41fc-91f7-a416e510f071\",\"response\":{\"authorizeUserResponse\":null}}}";

    @Test
    public void parseInvalidLoginResponse() throws Exception {

        LoginResponse loginResponse = JsonParser.ParseJsonForLogin(INVALID_LOGIN_RESPONSE);
        Assert.assertNotNull(loginResponse);
        Assert.assertNotNull(loginResponse.getTsspJsonResponse());
        Assert.assertEquals(loginResponse.getTsspJsonResponse().getStatuscode(),"010");
    }
}